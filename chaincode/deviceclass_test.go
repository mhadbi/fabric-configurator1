package main

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkGetExistingDeviceClass(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Existing Device Class " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetDeviceClassByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	dc := DeviceClass{}
	_ = json.Unmarshal(response.Payload, &dc)

	if dc.Uuid != uuid {
		t.Fail()
	}
}

func checkGetNonExistingDeviceClass(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get NON Existing Device Class " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetDeviceClassByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkUnregisterDeviceClass(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Unregister existing device class " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("UnregisterDeviceClassByID"), []byte(uuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkCreateDeviceClassWhenManufacturerDoesNotExist(t *testing.T, stub *shim.MockStub, uuid string, name string, prettyName string, manufacturerUuid string) {
	displayNewTest("Create Device Class when Manufacturer Org DOES NOT Exist")
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte(uuid), []byte(name), []byte(prettyName), []byte(manufacturerUuid)})
	if response.Status == shim.OK || response.Payload != nil {
		t.Fail()
	}
}
func checkCreateDeviceClassWhenManufacturerExist(t *testing.T, stub *shim.MockStub, uuid string, name string, prettyName string, manufacturerUuid string) {
	displayNewTest("Create Device Class when Manufacturer Exist")
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte(uuid), []byte(name), []byte(prettyName), []byte(manufacturerUuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllDeviceClass(t *testing.T, stub *shim.MockStub, objectType string, numberOfDeviceCreated int) {
	displayNewTest("GetAllByObjectType DeviceClass ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllDeviceClass"),
		[]byte(objectType)})

	var liste []DeviceClass
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil || len(liste) != numberOfDeviceCreated {
		t.Fail()
	}
}

func checkGetAllDeviceClassWithPage(t *testing.T, stub *shim.MockStub, pageSize string, offset string) {
	displayNewTest("GetAllByObjectTypeWithpage DeviceClass ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllDeviceClassWithPage"), []byte(pageSize), []byte(offset)})
	var liste []DeviceClass
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestCreateDeviceClassWhenManufacturerExist(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O0"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	checkCreateDeviceClassWhenManufacturerExist(t, stub, "DC1", "dc1name", "dc1pname", "O0")
}

func TestCreateDeviceClassWhenManufacturerDoesNotExist(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	checkCreateDeviceClassWhenManufacturerDoesNotExist(t, stub, "DC1", "dc1name", "dc1pname", "O1")
}

func TestGetExistingDeviceClass(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O0"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	checkCreateDeviceClassWhenManufacturerExist(t, stub, "DC0", "dc0name", "dc0pname", "O0")

	checkGetExistingDeviceClass(t, stub, "DC0")

}

func TestGetNonExistingDeviceClass(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	checkGetNonExistingDeviceClass(t, stub, "DC1")
}

func TestGetAllDeviceClass(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O0"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})

	checkCreateDeviceClassWhenManufacturerExist(t, stub, "DC0", "dc0name", "dc0pname", "O0")
	checkCreateDeviceClassWhenManufacturerExist(t, stub, "DC1", "dc1name", "dc1pname", "O0")

	checkGetAllDeviceClass(t, stub, "deviceclass", 2)
}

func TestGetAllDeviceClassWithPage(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O0"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O1"), []byte("SCHAIN1"), []byte("Scorechain1"), []byte("ISP1")})
	checkCreateDeviceClassWhenManufacturerExist(t, stub, "DC0", "dc0name", "dc0pname", "O0")
	checkCreateDeviceClassWhenManufacturerExist(t, stub, "DC1", "dc1name", "dc1pname", "O0")
	checkCreateDeviceClassWhenManufacturerExist(t, stub, "DC2", "dc2name", "dc2pname", "O1")
	checkCreateDeviceClassWhenManufacturerExist(t, stub, "DC3", "dc3name", "dc3pname", "O1")
	checkCreateDeviceClassWhenManufacturerExist(t, stub, "DC4", "dc4name", "dc4pname", "O1")
	checkCreateDeviceClassWhenManufacturerExist(t, stub, "DC5", "dc4name", "dc4pname", "O1")

	checkGetAllDeviceClassWithPage(t, stub, "100", "0")
}

func TestCheckUnregisterDeviceClass(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O0"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	checkCreateDeviceClassWhenManufacturerExist(t, stub, "DC0", "dc0name", "dc0pname", "O0")
	checkUnregisterDeviceClass(t, stub, "DC0")

}

func TestCheckUnregisterDeviceClassWithExistingPatchCOnfiguration(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O0"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	checkCreateDeviceClassWhenManufacturerExist(t, stub, "DC0", "dc0name", "dc0pname", "O0")

	//patch
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P1"), []byte("patch1"), []byte("Patch 1"),
		[]byte("today"), []byte("current"), []byte("http"), []byte("1.0"), []byte("DC0")})
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P2"), []byte("patch2"), []byte("Patch 2"),
		[]byte("today"), []byte("current"), []byte("http"), []byte("1.0"), []byte("DC0")})
	//patchFile
	checkCreatePatchFile(t, stub, "F1", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F2", "file1", "File #1", "http", "123", "1.0", "P1")

	//configuration
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W1"), []byte("a1"), []byte("current"), []byte("DC0")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W2"), []byte("a1"), []byte("current"), []byte("DC0")})
	//configurationFile
	checkCreateConfigurationFile(t, stub, "X1", "d", "current", "h", "h", "1.0", "W1")
	checkCreateConfigurationFile(t, stub, "X2", "d", "current", "h", "h", "1.0", "W1")

	checkUnregisterDeviceClass(t, stub, "DC0")

	checkGetAllPatchs(t, stub, "patch")
	checkGetAllConfiguration(t, stub, "configuration", 0)
	checkGetAllPatchFiles(t, stub, "patchfile")
	checkGetAllConfigurationFile(t, stub, "configurationfile", 0)

}
