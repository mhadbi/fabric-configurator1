package main

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkGetExistingNetworkTrafficEvidenceFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Existing NetworkTrafficEvidenceFile " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetNetworkTrafficEvidenceFileByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	dc := NetworkTrafficEvidenceFile{}
	_ = json.Unmarshal(response.Payload, &dc)

	if dc.Uuid != uuid {
		t.Fail()
	}
}

func checkGetNonExistingNetworkTrafficEvidenceFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get NON Existing NetworkTrafficEvidenceFile " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetNetworkTrafficEvidenceFileByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkCreateNetworkTrafficEvidenceFileWhenEvidenceDoesNotExist(t *testing.T, stub *shim.MockStub, uuid string,
	name string, prettyName string, evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string,
	attackStatus string, sourceIP string, destinationIP string, protocol string, port string, statusCode string, evidence string) {
	displayNewTest("Create NetworkTrafficEvidenceFile when Evidence Org DOES NOT Exist")
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateNetworkTrafficEvidenceFile"), []byte(uuid), []byte(name), []byte(prettyName), []byte(evidenceFileDataSourceTitle),
		[]byte(evidenceFileInformation), []byte(evidenceFileCreationDate), []byte(attackStatus), []byte(sourceIP),
		[]byte(destinationIP), []byte(protocol), []byte(port), []byte(statusCode), []byte(evidence)})
	if response.Status == shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t *testing.T, stub *shim.MockStub, uuid string,
	name string, prettyName string, evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string,
	attackStatus string, sourceIP string, destinationIP string, protocol string, port string, statusCode string, evidence string) {
	displayNewTest("Create NetworkTrafficEvidenceFile when Evidence Exist")
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateNetworkTrafficEvidenceFile"), []byte(uuid), []byte(name), []byte(prettyName), []byte(evidenceFileDataSourceTitle),
		[]byte(evidenceFileInformation), []byte(evidenceFileCreationDate), []byte(attackStatus), []byte(sourceIP),
		[]byte(destinationIP), []byte(protocol), []byte(port), []byte(statusCode), []byte(evidence)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllNetworkTrafficEvidenceFile(t *testing.T, stub *shim.MockStub, objectType string, numberOfDeviceCreated int) {
	displayNewTest("GetAllByObjectType NetworkTrafficEvidenceFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllNetworkTrafficEvidenceFile"),
		[]byte(objectType)})

	var liste []NetworkTrafficEvidenceFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil || len(liste) != numberOfDeviceCreated {
		t.Fail()
	}
}

func checkGetAllNetworkTrafficEvidenceFileWithPage(t *testing.T, stub *shim.MockStub, pageSize string, offset string) {
	displayNewTest("GetAllByObjectTypeWithpage NetworkTrafficEvidenceFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllNetworkTrafficEvidenceFileByPage"), []byte(pageSize), []byte(offset)})
	var liste []NetworkTrafficEvidenceFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NTE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E0")
}

func TestCreateNetworkTrafficEvidenceFileWhenEvidenceDoesNotExist(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NTE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E0")
}

func TestGetExistingNetworkTrafficEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NTE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E0")
	checkGetExistingNetworkTrafficEvidenceFile(t, stub, "NTE0")

}

func TestGetNonExistingNetworkTrafficEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	checkGetNonExistingNetworkTrafficEvidenceFile(t, stub, "NTE0")
}

func TestGetAllNetworkTrafficEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NTE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E0")
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NTE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E0")

	checkGetAllNetworkTrafficEvidenceFile(t, stub, "networkTrafficEvidenceFile", 2)
}

func TestGetAllNetworkTrafficEvidenceFileWithPage(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E1"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NTE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E0")
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NTE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E0")
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NDE2", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E0")
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NDE3", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E0")

	checkGetAllNetworkTrafficEvidenceFileWithPage(t, stub, "100", "0")
}
