package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//Organization declaration of the srtuct
type Organization struct {
	ObjectType         string
	Uuid               string
	Name               string
	PrettyName         string
	TypeOfOrganization string
}

func createOrganizationOnLedger(stub shim.ChaincodeStubInterface, objectType string, uuid string, name string, prettyName string,
	typeOfOrganization string) []byte {

	org := Organization{objectType, uuid, name, prettyName, typeOfOrganization}
	orgAsJSONBytes := makeBytesFromOrganization(stub, org)

	uuidIndexKey := createIndexKey(stub, uuid, "organization")
	putEntityInLedger(stub, uuidIndexKey, orgAsJSONBytes)
	return orgAsJSONBytes
}

func makeOrganizationFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) Organization {
	organization := Organization{}
	err := json.Unmarshal(bytes, &organization)
	panicErr(err)
	return organization
}

func makeBytesFromOrganization(stub shim.ChaincodeStubInterface, organization Organization) []byte {
	bytes, err := json.Marshal(organization)
	panicErr(err)
	return bytes
}

//CreateOrganization method to create an organization
func (t *Organization) CreateOrganization(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n CreateOrganization - Start")

	uuid := args[0]
	name := args[1]
	prettyName := args[2]
	typeOfOrganization := args[3]

	uuidIndexKey := createIndexKey(stub, uuid, "organization")
	if checkEntityExist(stub, uuidIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "organization")
	}

	createdOrg := createOrganizationOnLedger(stub, "organization", uuid, name, prettyName, typeOfOrganization)
	return succeed(stub, "organizationCreatedEvent", createdOrg)
}

//GetOrganizationByID method to get an organization by id
func (t *Organization) GetOrganizationByID(stub shim.ChaincodeStubInterface, args string) pb.Response {
	fmt.Println("\n GetOrganizationByID - Start", args)

	uuid := args

	uuidIndexKey := createIndexKey(stub, uuid, "organization")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "organization")
	}
	organizationAsBytes := getEntityFromLedger(stub, uuidIndexKey)

	return shim.Success(organizationAsBytes)
}

//UpdateOrganizationByID method to update an organization by id
func (t *Organization) UpdateOrganizationByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n UpdateOrganizationByID - Start")

	uuid := args[0]
	newName := args[1]
	newPrettyName := args[2]
	newTypeOfOrganization := args[3]

	uuidIndexKey := createIndexKey(stub, uuid, "organization")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "organization")
	}
	organizationAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	organization := makeOrganizationFromBytes(stub, organizationAsBytes)

	organization.Name = newName
	organization.PrettyName = newPrettyName
	organization.TypeOfOrganization = newTypeOfOrganization

	orgAsJSONBytes := makeBytesFromOrganization(stub, organization)

	putEntityInLedger(stub, uuidIndexKey, orgAsJSONBytes)
	return succeed(stub, "organizationUpdatedEvent", orgAsJSONBytes)

}

//UnregisterOrganizationByID method to unregister an organization by id
func (t *Organization) UnregisterOrganizationByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n UnregisterOrganizationByID - Start")
	uuid := args[0]

	uuidIndexKey := createIndexKey(stub, uuid, "organization")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "organization")
	}
	organizationAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	/* if organizationAsBytes == nil {
		return entityNotFoundMessage(stub, uuid)
	} */
	administrator := Administrator{}

	if organizationAsBytes == nil {
		fmt.Println("Impossible to delete non-existent organization")
		return entityNotFoundMessage(stub, uuid, "organization")
	}

	//delete all administration by this organization
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"administrator"})
	if err != nil {
		return shim.Error(err.Error())
	}
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		fmt.Println("\n UnregisterAdministrator - Start")
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		administratorAsBytes := getEntityFromLedger(stub, responseRange.Key)
		administrator = makeAdministratorFromBytes(stub, administratorAsBytes)

		if administrator.ParentOrganization.Uuid == uuid {
			adminUUIDIndexKey := createIndexKey(stub, administrator.EmployeeID, "administrator")
			deleteEntityFromLedger(stub, adminUUIDIndexKey)
			fmt.Println("Administrator " + administrator.EmployeeID + " was unregistered successfully")
		}
	}

	//delete organization
	deleteEntityFromLedger(stub, uuidIndexKey)

	fmt.Println("Organization " + uuid + " was unregistered successfully")
	return succeed(stub, "organizationUnregisteredEvent", []byte("{\"OrganizationUUID\":\""+uuid+"\"}"))
}

//GetAllOrganizations Method to get all organizations from the ledger
func (t *Organization) GetAllOrganizations(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllOrganizations:(%s)\n", args)
	objectType := args[0]
	organization := Organization{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []Organization
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		organizationAsBytes := getEntityFromLedger(stub, responseRange.Key)
		organization = makeOrganizationFromBytes(stub, organizationAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, organizationAsBytes)

		list = append(list, organization)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of organizations found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllOrganizationsByPage Method to get all organizations from the ledger
func (t *Organization) GetAllOrganizationsByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllOrganizationsByPage:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"organization"})
	panicErr(err)

	var list []Organization

	for i := 0; i < offset && objectTypeResultsIterator.HasNext() == true; i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		organizationAsBytes := getEntityFromLedger(stub, responseRange.Key)
		organization := makeOrganizationFromBytes(stub, organizationAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, organizationAsBytes)

		list = append(list, organization)
	}
	objectTypeResultsIterator.Close()

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of organizations found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
