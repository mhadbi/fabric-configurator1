package main

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//FunctionnalError definition of struct
type FunctionnalError struct {
	errorTag string
	errorMsg string
}

func (e *FunctionnalError) Error() string {
	return e.errorTag
}

//NewFunctionnalError method to define new functionnal error
func NewFunctionnalError(errorTag string, errorMsg string) *FunctionnalError {
	return &FunctionnalError{errorTag: errorTag, errorMsg: errorMsg}
}

func panicErr(err error) {
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
}

func checkEntityDoesNotExist(stub shim.ChaincodeStubInterface, uuid string) {
	response, err := stub.GetState(uuid)
	panicErr(err)

	if response != nil {
		errorMessage := "Entity with uuid " + uuid + " already exists in the ledger."
		fmt.Printf(errorMessage)
		panicErr(errors.New(errorMessage))
	}

}
func checkEntityExist(stub shim.ChaincodeStubInterface, uuid string) bool {
	response, err := stub.GetState(uuid)
	panicErr(err)

	if response == nil {
		return false
	}
	return true
}

func succeed(stub shim.ChaincodeStubInterface, eventMessage string, eventPayload []byte) pb.Response {
	stub.SetEvent(eventMessage, eventPayload)
	m := map[string]interface{}{}
	if err := json.Unmarshal(eventPayload, &m); err != nil {
		panic(err)
	}
	fmt.Printf("%q", m)
	return shim.Success([]byte("Success: " + eventMessage))
}

func getEntityFromLedger(stub shim.ChaincodeStubInterface, uuid string) []byte {
	entityAsBytes, err := stub.GetState(uuid)
	if err != nil {
		errorMessage := "Failed to get state for Entity with uuid " + uuid
		panicErr(errors.New(errorMessage))
	} else if entityAsBytes == nil {
		fmt.Println("Entity " + uuid + " not found. Returningss nil...")
		return entityAsBytes
	}

	fmt.Println("GET Entity with uuid " + uuid + " returned: ")
	fmt.Println(string(entityAsBytes))

	return entityAsBytes
}

func checkEntityExistsAndGet(stub shim.ChaincodeStubInterface, uuid string) []byte {
	response, err := stub.GetState(uuid)
	panicErr(err)

	if response == nil {
		errorMessage := "Entity with uuid " + uuid + " does not exist in the ledger"
		fmt.Printf(errorMessage)
		panicErr(errors.New(errorMessage))
	}
	return response
}

func deleteEntityFromLedger(stub shim.ChaincodeStubInterface, uuid string) {
	err := stub.DelState(uuid)
	panicErr(err)
	fmt.Println("DelState done")
}

func putEntityInLedger(stub shim.ChaincodeStubInterface, uuid string, payload []byte) {
	err := stub.PutState(uuid, payload)
	panicErr(err)
	fmt.Println("Put entity " + uuid + " in ledger")
}

func createIndexKey(stub shim.ChaincodeStubInterface, uuid string, objectType string) string {
	indexName := "objectType~uuid"
	uuidIndexKey, err := stub.CreateCompositeKey(indexName, []string{objectType, uuid})
	panicErr(err)
	return uuidIndexKey
}

func createIndexKey2(stub shim.ChaincodeStubInterface, args []string) string {
	indexName := "objectType~subObjectType~uuid"
	uuidIndexKey, err := stub.CreateCompositeKey(indexName, []string{args[0], args[1], args[2]})
	panicErr(err)
	return uuidIndexKey
}
func splitIndexKey(stub shim.ChaincodeStubInterface, compositeKey string) []string {
	_, key, err := stub.SplitCompositeKey(compositeKey)
	panicErr(err)
	return key
}

func entityNotFoundMessage(stub shim.ChaincodeStubInterface, uuid string, objectType string) pb.Response {
	errorMessage := objectType + " with uuid " + uuid + " not found in the ledger."
	errorType := ""
	switch objectType {
	case "organization":
		errorType = "noOrganizationFound"
	case "administrator":
		errorType = "noAdministratorFound"
	case "deviceclass":
		errorType = "noDeviceClassFound"
	case "device":
		errorType = "noDeviceFound"
	case "patch":
		errorType = "noPatchFound"
	case "configuration":
		errorType = "noConfigurationFound"
	case "patchfile":
		errorType = "noPatchFileFound"
	case "configurationfile":
		errorType = "noConfigurationFileFound"
	case "devicelog":
		errorType = "noDeviceLogFound"
	case "evidence":
		errorType = "noEvidenceFound"
	case "evidenceFile":
		errorType = "noEvidenceFileFound"
	case "osEvidenceFile":
		errorType = "noOSEvidenceFileFound"
	case "softwareEvidenceFile":
		errorType = "noSoftwareEvidenceFileFound"
	case "networkTrafficEvidenceFile":
		errorType = "noNetworkTrafficEvidenceFileFound"
	case "networkDiagnosticEvidenceFile":
		errorType = "noNetworkDiagnosticEvidenceFileFound"
	case "incidentTrackingEvidenceFile":
		errorType = "noIncidentTrackingEvidenceFileFound"
	}

	err := NewFunctionnalError(errorType, errorMessage)
	fmt.Println(errorMessage)
	return shim.Error(err.Error())

}

func entityAlreadyExistMessage(stub shim.ChaincodeStubInterface, uuid string, objectType string) pb.Response {

	errorMessage := "Entity with uuid " + uuid + " already exists in the ledger."
	errorType := ""
	switch objectType {
	case "organization":
		errorType = "organizationAlreadyExist"
	case "administrator":
		errorType = "administratorAlreadyExist"
	case "deviceclass":
		errorType = "deviceClassAlreadyExist"
	case "device":
		errorType = "deviceAlreadyExist"
	case "patch":
		errorType = "patchAlreadyExist"
	case "configuration":
		errorType = "configurationAlreadyExist"
	case "patchfile":
		errorType = "patchFileAlreadyExist"
	case "configurationfile":
		errorType = "configurationFileAlreadyExist"
	case "devicelog":
		errorType = "deviceLogAlreadyExist"
	case "evidence":
		errorType = "evidenceAlreadyExist"
	case "osEvidenceFile":
		errorType = "osEvidenceFileAlreadyExist"
	case "evidenceFile":
		errorType = "evidenceFileAlreadyExist"
	case "softwareEvidenceFile":
		errorType = "softwareEvidenceFileAlreadyExist"
	case "networkTrafficEvidenceFile":
		errorType = "networkTrafficEvidenceFileAlreadyExist"
	case "networkDiagnosticEvidenceFile":
		errorType = "networkDiagnosticEvidenceFileAlreadyExist"
	case "incidentTrackingEvidenceFile":
		errorType = "incidentTrackingEvidenceFileAlreadyExist"
	}

	err := NewFunctionnalError(errorType, errorMessage)
	fmt.Println(errorMessage)
	return shim.Error(err.Error())
}
