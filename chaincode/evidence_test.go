package main

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkCreateNewEvidence(t *testing.T, stub *shim.MockStub, uuid string, name string, prettyName string,
	evidenceDate string, evidenceURL string, targetedVulnerability string, typeOfAttack string) {
	displayNewTest("Create Evidence Test When Evidence does not exist")

	response := stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"),
		[]byte(uuid), []byte(name), []byte(prettyName), []byte(evidenceDate), []byte(evidenceURL),
		[]byte(targetedVulnerability), []byte(typeOfAttack)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetExistingEvidence(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Existing Evidence " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetEvidenceByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	org := Evidence{}
	_ = json.Unmarshal(response.Payload, &org)

	if org.Uuid != uuid {
		t.Fail()
	}
}

func checkGetAllEvidences(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType Evidence ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllEvidences"),
		[]byte(objectType)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllEvidencesByPage(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType Evidence ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllEvidencesByPage"), []byte("4"), []byte("0")})

	response = stub.MockInvoke("1", [][]byte{[]byte("GetAllEvidencesByPage"), []byte("2"), []byte("3")})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestCreateEvidence(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	checkCreateNewEvidence(t, stub, "E0", "EvidenceTest", "Evidence Test", "20/10/2019", "http://cyber-trust.fr/evidenceURL", "TargetedVulnerability", "TypeOfAttack")
}

func TestGetEvidenceByKey(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	checkCreateNewEvidence(t, stub, "E0", "EvidenceTest", "Evidence Test", "20/10/2019", "http://cyber-trust.fr/evidenceURL", "TargetedVulnerability", "TypeOfAttack")
	checkGetExistingEvidence(t, stub, "E0")
}

func mockTestValue1(t *testing.T, stub *shim.MockStub) {
	checkCreateNewEvidence(t, stub, "E0", "EvidenceTest", "Evidence Test", "20/10/2019", "http://cyber-trust.fr/evidenceURL", "TargetedVulnerability", "TypeOfAttack")
	checkCreateNewEvidence(t, stub, "O1", "EvidenceTest", "Evidence Test", "20/10/2019", "http://cyber-trust.fr/evidenceURL", "TargetedVulnerability", "TypeOfAttack")
	checkCreateNewEvidence(t, stub, "O2", "EvidenceTest", "Evidence Test", "20/10/2019", "http://cyber-trust.fr/evidenceURL", "TargetedVulnerability", "TypeOfAttack")
	checkCreateNewEvidence(t, stub, "O3", "EvidenceTest", "Evidence Test", "20/10/2019", "http://cyber-trust.fr/evidenceURL", "TargetedVulnerability", "TypeOfAttack")
	checkCreateNewEvidence(t, stub, "O4", "EvidenceTest", "Evidence Test", "20/10/2019", "http://cyber-trust.fr/evidenceURL", "TargetedVulnerability", "TypeOfAttack")
}

func mockTestValue2(t *testing.T, stub *shim.MockStub) {
	checkCreateNewEvidence(t, stub, "O5", "EvidenceTest", "Evidence Test", "20/10/2019", "http://cyber-trust.fr/evidenceURL", "TargetedVulnerability", "TypeOfAttack")
	checkCreateNewEvidence(t, stub, "O6", "EvidenceTest", "Evidence Test", "20/10/2019", "http://cyber-trust.fr/evidenceURL", "TargetedVulnerability", "TypeOfAttack")
	checkCreateNewEvidence(t, stub, "O7", "EvidenceTest", "Evidence Test", "20/10/2019", "http://cyber-trust.fr/evidenceURL", "TargetedVulnerability", "TypeOfAttack")
	checkCreateNewEvidence(t, stub, "O8", "EvidenceTest", "Evidence Test", "20/10/2019", "http://cyber-trust.fr/evidenceURL", "TargetedVulnerability", "TypeOfAttack")
	checkCreateNewEvidence(t, stub, "O9", "EvidenceTest", "Evidence Test", "20/10/2019", "http://cyber-trust.fr/evidenceURL", "TargetedVulnerability", "TypeOfAttack")
}
func TestGetAllEvidences(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	mockTestValue1(t, stub)
	checkGetAllEvidences(t, stub, "organization")
}

func TestGetAllEvidencesByPage(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	mockTestValue1(t, stub)
	mockTestValue2(t, stub)

	checkGetAllEvidencesByPage(t, stub, "organization")
}
