package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//Administrator dleclaration of the struct
type Administrator struct {
	ObjectType         string
	EmployeeID         string
	ParentOrganization Organization
	AccreditationLevel string
}

func createAdministratorOnLedger(stub shim.ChaincodeStubInterface, objectType string, employeeID string, parentOrganization Organization, accreditationLevel string) []byte {
	admin := Administrator{objectType, employeeID, parentOrganization, accreditationLevel}
	adminAsJSONBytes := makeBytesFromAdministrator(stub, admin)

	uuidIndexKey := createIndexKey(stub, admin.EmployeeID, admin.ObjectType)

	putEntityInLedger(stub, uuidIndexKey, adminAsJSONBytes)
	return adminAsJSONBytes
}

func makeAdministratorFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) Administrator {
	administrator := Administrator{}
	err := json.Unmarshal(bytes, &administrator)
	panicErr(err)
	return administrator
}

func makeBytesFromAdministrator(stub shim.ChaincodeStubInterface, administrator Administrator) []byte {
	bytes, err := json.Marshal(administrator)
	panicErr(err)
	return bytes
}

//CreateAdministrator method to create an administrator
func (t *Administrator) CreateAdministrator(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nCreateAdministrator - Start")

	employeeID := args[0]
	parentOrgUUID := args[1]
	accreditationLevel := args[2]

	adminIndexKey := createIndexKey(stub, employeeID, "administrator")
	if checkEntityExist(stub, adminIndexKey) == true {
		return entityAlreadyExistMessage(stub, employeeID, "administrator")
	}

	uuidIndexKey := createIndexKey(stub, parentOrgUUID, "organization")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, parentOrgUUID, "organization")
	}
	parentOrgAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	parentOrg := makeOrganizationFromBytes(stub, parentOrgAsBytes)

	createdAdmin := createAdministratorOnLedger(stub, "administrator", employeeID, parentOrg, accreditationLevel)
	return succeed(stub, "administratorCreatedEvent", createdAdmin)
}

//GetAdministratorByEmployeeID method to get all employee by id
func (t *Administrator) GetAdministratorByEmployeeID(stub shim.ChaincodeStubInterface, args string) pb.Response {
	fmt.Println("\n GetAdministratorByEmployeeID  - Start")

	employeeID := args

	uuidIndexKey := createIndexKey(stub, employeeID, "administrator")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, employeeID, "administrator")
	}
	administratorAsBytes := getEntityFromLedger(stub, uuidIndexKey)

	return shim.Success(administratorAsBytes)
}

//UpdateAdministratorByID method to update an organization by id
func (t *Administrator) UpdateAdministratorByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n UpdateAdministratorByID - Start")

	employeeID := args[0]
	newParentOrganization := args[1]
	newAccreditationLevel := args[2]

	uuidIndexKey := createIndexKey(stub, employeeID, "administrator")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, employeeID, "administrator")
	}
	administratorAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	administrator := makeAdministratorFromBytes(stub, administratorAsBytes)

	orgUUIDIndexKey := createIndexKey(stub, newParentOrganization, "organization")
	if checkEntityExist(stub, orgUUIDIndexKey) == false {
		return entityNotFoundMessage(stub, newParentOrganization, "organization")
	}
	parentOrgAsBytes := getEntityFromLedger(stub, orgUUIDIndexKey)
	parentOrg := makeOrganizationFromBytes(stub, parentOrgAsBytes)

	administrator.AccreditationLevel = newAccreditationLevel
	administrator.ParentOrganization = parentOrg

	adminAsJSONBytes := makeBytesFromAdministrator(stub, administrator)

	putEntityInLedger(stub, uuidIndexKey, adminAsJSONBytes)
	return succeed(stub, "administratorUpdatedEvent", adminAsJSONBytes)

}

//UnregisterAdministratorByEmployeeID method to unregister administrator by id
func (t *Administrator) UnregisterAdministratorByEmployeeID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nUnregisterAdministratorByEmployeeID - Start")
	employeeID := args[0]

	uuidIndexKey := createIndexKey(stub, employeeID, "administrator")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, employeeID, "administrator")
	}
	administratorAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	makeAdministratorFromBytes(stub, administratorAsBytes)

	deleteEntityFromLedger(stub, uuidIndexKey)
	fmt.Println("Administrator " + employeeID + " was unregistered successfully")
	return succeed(stub, "administratorUnregisteredEvent", []byte("{\"uuid\":\""+employeeID+"\"}"))
}

//GetAllAdministrators method to get all administrators from ledger
func (t *Administrator) GetAllAdministrators(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllAdministrators - Start:(%s)\n", args)

	objectType := args[0]
	administrator := Administrator{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []Administrator
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		administratorAsBytes := getEntityFromLedger(stub, responseRange.Key)
		administrator = makeAdministratorFromBytes(stub, administratorAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, administratorAsBytes)

		list = append(list, administrator)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of administrators found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllAdministratorsByOrganization method to get all administrators by organization
func (t *Administrator) GetAllAdministratorsByOrganization(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllAdministratorsByOrganization - Start:(%s)\n", args)

	objectType := args[0]
	organizationUUID := args[1]
	administrator := Administrator{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []Administrator
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		administratorAsBytes := getEntityFromLedger(stub, responseRange.Key)
		administrator = makeAdministratorFromBytes(stub, administratorAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, administratorAsBytes)
		if administrator.ParentOrganization.Uuid == organizationUUID {
			list = append(list, administrator)
		}
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of administrators By Organization found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllAdministratorsByPage method to get all administrators from ledger
func (t *Administrator) GetAllAdministratorsByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllAdministratorsByPage - Start:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"administrator"})
	panicErr(err)

	var list []Administrator

	for i := 0; i < offset && objectTypeResultsIterator.HasNext(); i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		administratorAsBytes := getEntityFromLedger(stub, responseRange.Key)
		administrator := makeAdministratorFromBytes(stub, administratorAsBytes)
		fmt.Printf("\n- found from uuid:%s response:%s\n", responseRange.Key, administratorAsBytes)

		list = append(list, administrator)
	}
	objectTypeResultsIterator.Close()

	newAdmins, err := json.Marshal(list)
	panicErr(err)
	fmt.Println("")
	fmt.Println("")
	fmt.Printf("\n List of Administrators found:\n%s\n", newAdmins)
	return shim.Success([]byte(newAdmins))
}
