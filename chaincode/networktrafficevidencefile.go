package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type NetworkTrafficEvidenceFile struct {
	ObjectType    string
	SubObjectType string
	Uuid          string
	SourceIP      string
	DestinationIP string
	Protocol      string
	Port          string
	StatusCode    string
	EvidenceFile  EvidenceFile
}

func createNetworkTrafficEvidenceFileOnLedger(stub shim.ChaincodeStubInterface, objectType string, uuid string, name string, prettyName string,
	evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string, attackStatus string,
	sourceIP string, destinationIP string, protocol string, port string, statusCode string, evidence Evidence) []byte {

	evidenceFile := EvidenceFile{name, prettyName, evidenceFileDataSourceTitle,
		evidenceFileInformation, evidenceFileCreationDate, attackStatus, evidence}

	networkTrafficEvidenceFile := NetworkTrafficEvidenceFile{objectType, "networkTrafficEvidenceFile", uuid, sourceIP, destinationIP,
		protocol, port, statusCode, evidenceFile}
	networkTrafficEvidenceFileAsJSONBytes := makeBytesFromNetworkTrafficEvidenceFile(stub, networkTrafficEvidenceFile)
	uuidIndexKey2 := createIndexKey2(stub, []string{"evidenceFile", "networkTrafficEvidenceFile", uuid})
	putEntityInLedger(stub, uuidIndexKey2, networkTrafficEvidenceFileAsJSONBytes)

	return networkTrafficEvidenceFileAsJSONBytes
}

func makeNetworkTrafficEvidenceFileFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) NetworkTrafficEvidenceFile {
	networkTrafficEvidenceFile := NetworkTrafficEvidenceFile{}
	err := json.Unmarshal(bytes, &networkTrafficEvidenceFile)
	panicErr(err)
	return networkTrafficEvidenceFile
}

func makeBytesFromNetworkTrafficEvidenceFile(stub shim.ChaincodeStubInterface, networkTrafficEvidenceFile NetworkTrafficEvidenceFile) []byte {
	bytes, err := json.Marshal(networkTrafficEvidenceFile)
	panicErr(err)
	return bytes
}

//CreateNetworkTrafficEvidenceFile method to create an networkTrafficEvidenceFile
func (t *NetworkTrafficEvidenceFile) CreateNetworkTrafficEvidenceFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n CreateNetworkTrafficEvidenceFile - Start")
	uuid := args[0]
	name := args[1]
	prettyName := args[2]
	evidenceFileDataSourceTitle := args[3]
	evidenceFileInformation := args[4]
	evidenceFileCreationDate := args[5]
	attackStatus := args[6]
	sourceIP := args[7]
	destinationIP := args[8]
	protocol := args[9]
	port := args[10]
	statusCode := args[11]
	evidence := args[12]

	uuidIndexKey := createIndexKey2(stub, []string{"evidenceFile", "networkTrafficEvidenceFile", uuid})
	if checkEntityExist(stub, uuidIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "evidenceFile")
	}
	evidenceIndexKey := createIndexKey(stub, evidence, "evidence")
	if checkEntityExist(stub, evidenceIndexKey) == false {
		return entityNotFoundMessage(stub, evidence, "evidence")
	}
	evidenceAsBytes := getEntityFromLedger(stub, evidenceIndexKey)
	evidenceUUID := makeEvidenceFromBytes(stub, evidenceAsBytes)
	createdNetworkTrafficEvidenceFile := createNetworkTrafficEvidenceFileOnLedger(stub, "evidenceFile", uuid, name,
		prettyName, evidenceFileDataSourceTitle, evidenceFileInformation, evidenceFileCreationDate, attackStatus,
		sourceIP, destinationIP, protocol, port, statusCode, evidenceUUID)
	return succeed(stub, "networkTrafficEvidenceFileCreatedEvent", createdNetworkTrafficEvidenceFile)
}

//GetNetworkTrafficEvidenceFileByID method to get an networkTrafficEvidenceFile by id
func (t *NetworkTrafficEvidenceFile) GetNetworkTrafficEvidenceFileByID(stub shim.ChaincodeStubInterface, args string) pb.Response {
	fmt.Println("\n GetNetworkTrafficEvidenceFileByID - Start", args)

	uuid := args

	uuidIndexKey := createIndexKey2(stub, []string{"evidenceFile", "networkTrafficEvidenceFile", uuid})
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "evidenceFile")
	}
	networkTrafficEvidenceFileAsBytes := getEntityFromLedger(stub, uuidIndexKey)

	return shim.Success(networkTrafficEvidenceFileAsBytes)
}

//GetAllNetworkTrafficEvidenceFile Method to get all networkTrafficEvidenceFiles from the ledger
func (t *NetworkTrafficEvidenceFile) GetAllNetworkTrafficEvidenceFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllNetworkTrafficEvidenceFile:(%s)\n", args)
	objectType := args[0]
	networkTrafficEvidenceFile := NetworkTrafficEvidenceFile{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~subObjectType~uuid", []string{objectType, "networkTrafficEvidenceFile"})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []NetworkTrafficEvidenceFile
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		networkTrafficEvidenceFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		networkTrafficEvidenceFile = makeNetworkTrafficEvidenceFileFromBytes(stub, networkTrafficEvidenceFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, networkTrafficEvidenceFileAsBytes)

		list = append(list, networkTrafficEvidenceFile)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of networkTrafficEvidenceFiles found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllNetworkTrafficEvidenceFileByPage Method to get all networkTrafficEvidenceFiles from the ledger
func (t *NetworkTrafficEvidenceFile) GetAllNetworkTrafficEvidenceFileByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllNetworkTrafficEvidenceFilesByPage:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~subObjectType~uuid", []string{"evidenceFile", "networkTrafficEvidenceFile"})
	panicErr(err)

	var list []NetworkTrafficEvidenceFile

	for i := 0; i < offset && objectTypeResultsIterator.HasNext() == true; i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		networkTrafficEvidenceFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		networkTrafficEvidenceFile := makeNetworkTrafficEvidenceFileFromBytes(stub, networkTrafficEvidenceFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, networkTrafficEvidenceFileAsBytes)

		list = append(list, networkTrafficEvidenceFile)
	}
	objectTypeResultsIterator.Close()

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of networkTrafficEvidenceFiles found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
