package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//DeviceLog declaration of struct
type DeviceLog struct {
	ObjectType      string
	Uuid            string
	Name            string
	TypeOfDeviceLog string
	Format          string
	DateStart       string
	DateEnd         string
	DateOfficial    string
	StorageURL      string
	FileHash        string
	CreatorDevice   Device
}

func createDeviceLogOnLedger(stub shim.ChaincodeStubInterface, objectType string, uuid string, name string, typeOfDeviceLog string,
	format string, dateStart string, dateEnd string, dateOfficial string, storageURL string,
	fileHash string, creatorDevice Device) []byte {
	deviceLog := DeviceLog{objectType, uuid, name, typeOfDeviceLog, format, dateStart, dateEnd, dateOfficial, storageURL,
		fileHash, creatorDevice}
	deviceLogAsJSONBytes := makeBytesFromDeviceLog(stub, deviceLog)

	uuidIndexKey := createIndexKey(stub, deviceLog.Uuid, deviceLog.ObjectType)

	putEntityInLedger(stub, uuidIndexKey, deviceLogAsJSONBytes)
	return deviceLogAsJSONBytes
}

func makeDeviceLogFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) DeviceLog {
	deviceLog := DeviceLog{}
	err := json.Unmarshal(bytes, &deviceLog)
	panicErr(err)
	return deviceLog
}

func makeBytesFromDeviceLog(stub shim.ChaincodeStubInterface, deviceLog DeviceLog) []byte {
	bytes, err := json.Marshal(deviceLog)
	panicErr(err)
	return bytes
}

//CreateDeviceLog method to create an deviceLog
func (t *DeviceLog) CreateDeviceLog(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nCreateDeviceLog - Start")

	uuid := args[0]
	name := args[1]
	typeOfDeviceLog := args[2]
	format := args[3]
	dateStart := args[4]
	dateEnd := args[5]
	dateOfficial := args[6]
	storageURL := args[7]
	fileHash := args[8]
	creatorDeviceUUID := args[9]

	deviceLogIndexKey := createIndexKey(stub, uuid, "devicelog")
	if checkEntityExist(stub, deviceLogIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "devicelog")
	}

	uuidIndexKey := createIndexKey(stub, creatorDeviceUUID, "device")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, creatorDeviceUUID, "device")
	}
	creatorDeviceAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	creatorDevice := makeDeviceFromBytes(stub, creatorDeviceAsBytes)

	createdDeviceLog := createDeviceLogOnLedger(stub, "devicelog", uuid, name, typeOfDeviceLog, format, dateStart, dateEnd,
		dateOfficial, storageURL, fileHash, creatorDevice)
	return succeed(stub, "deviceLogCreatedEvent", createdDeviceLog)
}

//GetDeviceLogByID method to get all employee by id
func (t *DeviceLog) GetDeviceLogByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n GetDeviceLogByID  - Start")

	uuid := args[0]

	uuidIndexKey := createIndexKey(stub, uuid, "devicelog")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "devicelog")
	}
	deviceLogAsBytes := getEntityFromLedger(stub, uuidIndexKey)

	return shim.Success(deviceLogAsBytes)
}

//GetAllDeviceLog method to get all deviceLogs from ledger
func (t *DeviceLog) GetAllDeviceLog(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllDeviceLog - Start:(%s)\n", args)

	objectType := args[0]
	deviceLog := DeviceLog{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []DeviceLog
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		deviceLogAsBytes := getEntityFromLedger(stub, responseRange.Key)
		deviceLog = makeDeviceLogFromBytes(stub, deviceLogAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, deviceLogAsBytes)

		list = append(list, deviceLog)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of deviceLogs found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
