package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//PatchFile Definition of the struct
type PatchFile struct {
	ObjectType string
	Uuid       string
	Name       string
	PrettyName string
	FileURL    string
	FileHash   string
	Version    string
	Patch      Patch
}

func createPatchFileOnLedger(stub shim.ChaincodeStubInterface, objectType string, uuid string, name string,
	prettyName string, fileURL string, fileHash string, version string, patchUUID Patch) []byte {
	patchFile := PatchFile{objectType, uuid, name, prettyName, fileURL, fileHash, version, patchUUID}
	patchFileAsBytes := makeBytesFromPatchFile(stub, patchFile)

	uuidIndexKey := createIndexKey(stub, patchFile.Uuid, "patchfile")
	putEntityInLedger(stub, string(uuidIndexKey), patchFileAsBytes)
	return patchFileAsBytes
}

func makePatchFileFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) PatchFile {
	patchFile := PatchFile{}
	err := json.Unmarshal(bytes, &patchFile)
	panicErr(err)
	return patchFile
}

func makeBytesFromPatchFile(stub shim.ChaincodeStubInterface, patchFile PatchFile) []byte {
	bytes, err := json.Marshal(patchFile)
	panicErr(err)
	return bytes
}

//CreatePatchFile method to create patchfile
func (t *PatchFile) CreatePatchFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nCreatePatchFile - Start")

	uuid := args[0]
	name := args[1]
	prettyName := args[2]
	fileURL := args[3]
	fileHash := args[4]
	version := args[5]
	patchUUID := args[6]

	uuidIndexKey := createIndexKey(stub, uuid, "patchfile")
	if checkEntityExist(stub, uuidIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "patchfile")
	}

	patchUUIDIndexKey := createIndexKey(stub, patchUUID, "patch")
	if checkEntityExist(stub, patchUUIDIndexKey) == false {
		return entityNotFoundMessage(stub, patchUUID, "patch")
	}
	patchAsBytes := getEntityFromLedger(stub, patchUUIDIndexKey)
	patch := makePatchFromBytes(stub, patchAsBytes)

	patchFileAsBytes := createPatchFileOnLedger(stub, "patchfile", uuid, name, prettyName, fileURL, fileHash, version, patch)
	return succeed(stub, "patchFileCreatedEvent", patchFileAsBytes)
}

//GetPatchFileByID method to get patchfile by id
func (t *PatchFile) GetPatchFileByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nGetPatchFileByID - Start")

	uuid := args[0]
	uuidIndexKey := createIndexKey(stub, uuid, "patchfile")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "patchfile")
	}
	patchFileAsBytes := getEntityFromLedger(stub, string(uuidIndexKey))
	return shim.Success(patchFileAsBytes)
}

//UpdatePatchFileByID method to update patchfile by id
func (t *PatchFile) UpdatePatchFileByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n UpdatePatchFileByID - Start")

	uuid := args[0]
	newName := args[1]
	newPrettyName := args[2]
	newFileURL := args[3]
	newFileHash := args[4]
	newVersion := args[5]
	newPatchUUID := args[6]

	uuidIndexKey := createIndexKey(stub, uuid, "patchfile")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "patchfile")
	}
	patchFileAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	patchFile := makePatchFileFromBytes(stub, patchFileAsBytes)

	patchFile.Name = newName
	patchFile.PrettyName = newPrettyName
	patchFile.FileURL = newFileURL
	patchFile.FileHash = newFileHash
	patchFile.Version = newVersion

	patchIndexKey := createIndexKey(stub, newPatchUUID, "patch")
	if checkEntityExist(stub, patchIndexKey) == false {
		return entityNotFoundMessage(stub, newPatchUUID, "patch")
	}
	patchAsBytes := getEntityFromLedger(stub, patchIndexKey)
	patch := makePatchFromBytes(stub, patchAsBytes)
	patchFile.Patch = patch

	patchFileAsJSONBytes := makeBytesFromPatchFile(stub, patchFile)

	putEntityInLedger(stub, uuidIndexKey, patchFileAsJSONBytes)
	return succeed(stub, "patchFileUpdatedEvent", patchFileAsJSONBytes)

}

//UnregisterPatchFileByID method to unregister patchfile by id
func UnregisterPatchFileByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	fmt.Println("\n UnregisterPatchFileByID - Start")
	uuid := args[0]

	uuidIndexKey := createIndexKey(stub, uuid, "patchfile")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "patchfile")
	}

	deleteEntityFromLedger(stub, uuidIndexKey)
	fmt.Println("Patch File " + uuid + " along with all patch files were successfully unregistered")
	return succeed(stub, "patchFileUnregisteredEvent", []byte("{\"PatchFileUUID\":\""+uuid+"\"}"))
}

//GetAllPatchFiles Method to get all PatchFiles from the ledger
func (t *PatchFile) GetAllPatchFiles(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllPatchFiles:(%s)\n", args)
	objectType := args[0]
	patchFile := PatchFile{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []PatchFile
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		patchFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		patchFile = makePatchFileFromBytes(stub, patchFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, patchFileAsBytes)

		list = append(list, patchFile)
	}

	newPatchFiles, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of patchFiles found:\n%s\n", newPatchFiles)
	return shim.Success([]byte(newPatchFiles))
}

//GetAllPatchFilesByPage Method to get all patchFiles from the ledger
func (t *PatchFile) GetAllPatchFilesByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("GetAllPatchFilesByPage:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"patchfile"})
	panicErr(err)

	var list []PatchFile

	for i := 0; i < offset && objectTypeResultsIterator.HasNext(); i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		patchFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		patchFile := makePatchFileFromBytes(stub, patchFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, patchFileAsBytes)

		list = append(list, patchFile)
	}
	objectTypeResultsIterator.Close()

	newPatchFiles, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of patchFiles found:\n%s\n", newPatchFiles)
	return shim.Success([]byte(newPatchFiles))
}

//GetAllPatchFilesByPatch method to get all patchfiles by patch
func (t *PatchFile) GetAllPatchFilesByPatch(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllPatchFilesByPatch:(%s)\n", args)
	objectType := args[0]
	patchUUID := args[1]
	patchFile := PatchFile{}

	uuidIndexKey := createIndexKey(stub, patchUUID, "patch")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, patchUUID, "patch")
	}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []PatchFile
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		patchFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		patchFile = makePatchFileFromBytes(stub, patchFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, patchFileAsBytes)
		if patchFile.Patch.Uuid == patchUUID {
			list = append(list, patchFile)
		}

	}

	newPatchFiles, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of patchFiles found:\n%s\n", newPatchFiles)
	return shim.Success([]byte(newPatchFiles))
}
