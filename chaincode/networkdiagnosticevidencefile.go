package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type NetworkDiagnosticEvidenceFile struct {
	ObjectType                   string
	SubObjectType                string
	Uuid                         string
	DiagnosticApplicationName    string
	DiagnosticApplicationVersion string
	DiagnosticApplicationResult  string
	EvidenceFile                 EvidenceFile
}

func createNetworkDiagnosticEvidenceFileOnLedger(stub shim.ChaincodeStubInterface, objectType string, uuid string, name string, prettyName string,
	evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string, attackStatus string,
	diagnosticApplicationName string, diagnosticApplicationVersion string, diagnosticApplicationResult string, evidence Evidence) []byte {

	evidenceFile := EvidenceFile{name, prettyName, evidenceFileDataSourceTitle,
		evidenceFileInformation, evidenceFileCreationDate, attackStatus, evidence}

	networkDiagnosticEvidenceFile := NetworkDiagnosticEvidenceFile{objectType, "networkDiagnosticEvidenceFile", uuid, diagnosticApplicationName, diagnosticApplicationVersion, diagnosticApplicationResult, evidenceFile}
	networkDiagnosticEvidenceFileAsJSONBytes := makeBytesFromNetworkDiagnosticEvidenceFile(stub, networkDiagnosticEvidenceFile)
	uuidIndexKey2 := createIndexKey2(stub, []string{"evidenceFile", "networkDiagnosticEvidenceFile", uuid})
	putEntityInLedger(stub, uuidIndexKey2, networkDiagnosticEvidenceFileAsJSONBytes)

	return networkDiagnosticEvidenceFileAsJSONBytes
}

func makeNetworkDiagnosticEvidenceFileFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) NetworkDiagnosticEvidenceFile {
	networkDiagnosticEvidenceFile := NetworkDiagnosticEvidenceFile{}
	err := json.Unmarshal(bytes, &networkDiagnosticEvidenceFile)
	panicErr(err)
	return networkDiagnosticEvidenceFile
}

func makeBytesFromNetworkDiagnosticEvidenceFile(stub shim.ChaincodeStubInterface, networkDiagnosticEvidenceFile NetworkDiagnosticEvidenceFile) []byte {
	bytes, err := json.Marshal(networkDiagnosticEvidenceFile)
	panicErr(err)
	return bytes
}

//CreateNetworkDiagnosticEvidenceFile method to create an networkDiagnosticEvidenceFile
func (t *NetworkDiagnosticEvidenceFile) CreateNetworkDiagnosticEvidenceFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n CreateNetworkDiagnosticEvidenceFile - Start")
	uuid := args[0]
	name := args[1]
	prettyName := args[2]
	evidenceFileDataSourceTitle := args[3]
	evidenceFileInformation := args[4]
	evidenceFileCreationDate := args[5]
	attackStatus := args[6]
	diagnosticApplicationName := args[7]
	diagnosticApplicationVersion := args[8]
	diagnosticApplicationResult := args[9]
	evidence := args[10]

	uuidIndexKey := createIndexKey2(stub, []string{"evidenceFile", "networkDiagnosticEvidenceFile", uuid})
	if checkEntityExist(stub, uuidIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "evidenceFile")
	}
	evidenceIndexKey := createIndexKey(stub, evidence, "evidence")
	if checkEntityExist(stub, evidenceIndexKey) == false {
		return entityNotFoundMessage(stub, evidence, "evidence")
	}
	evidenceAsBytes := getEntityFromLedger(stub, evidenceIndexKey)
	evidenceUUID := makeEvidenceFromBytes(stub, evidenceAsBytes)

	createdNetworkDiagnosticEvidenceFile := createNetworkDiagnosticEvidenceFileOnLedger(stub, "evidenceFile", uuid, name,
		prettyName, evidenceFileDataSourceTitle, evidenceFileInformation, evidenceFileCreationDate, attackStatus, diagnosticApplicationName, diagnosticApplicationVersion, diagnosticApplicationResult, evidenceUUID)
	return succeed(stub, "networkDiagnosticEvidenceFileCreatedEvent", createdNetworkDiagnosticEvidenceFile)
}

//GetNetworkDiagnosticEvidenceFileByID method to get an networkDiagnosticEvidenceFile by id
func (t *NetworkDiagnosticEvidenceFile) GetNetworkDiagnosticEvidenceFileByID(stub shim.ChaincodeStubInterface, args string) pb.Response {
	fmt.Println("\n GetNetworkDiagnosticEvidenceFileByID - Start", args)

	uuid := args

	uuidIndexKey := createIndexKey2(stub, []string{"evidenceFile", "networkDiagnosticEvidenceFile", uuid})
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "evidenceFile")
	}
	networkDiagnosticEvidenceFileAsBytes := getEntityFromLedger(stub, uuidIndexKey)

	return shim.Success(networkDiagnosticEvidenceFileAsBytes)
}

//GetAllNetworkDiagnosticEvidenceFile Method to get all networkDiagnosticEvidenceFiles from the ledger
func (t *NetworkDiagnosticEvidenceFile) GetAllNetworkDiagnosticEvidenceFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllNetworkDiagnosticEvidenceFile:(%s)\n", args)
	objectType := args[0]
	networkDiagnosticEvidenceFile := NetworkDiagnosticEvidenceFile{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~subObjectType~uuid", []string{objectType, "networkDiagnosticEvidenceFile"})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []NetworkDiagnosticEvidenceFile
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		networkDiagnosticEvidenceFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		networkDiagnosticEvidenceFile = makeNetworkDiagnosticEvidenceFileFromBytes(stub, networkDiagnosticEvidenceFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, networkDiagnosticEvidenceFileAsBytes)

		list = append(list, networkDiagnosticEvidenceFile)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of networkDiagnosticEvidenceFiles found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllNetworkDiagnosticEvidenceFileByPage Method to get all networkDiagnosticEvidenceFiles from the ledger
func (t *NetworkDiagnosticEvidenceFile) GetAllNetworkDiagnosticEvidenceFileByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllNetworkDiagnosticEvidenceFilesByPage:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~subObjectType~uuid", []string{"evidenceFile", "networkDiagnosticEvidenceFile"})
	panicErr(err)

	var list []NetworkDiagnosticEvidenceFile

	for i := 0; i < offset && objectTypeResultsIterator.HasNext() == true; i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		networkDiagnosticEvidenceFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		networkDiagnosticEvidenceFile := makeNetworkDiagnosticEvidenceFileFromBytes(stub, networkDiagnosticEvidenceFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, networkDiagnosticEvidenceFileAsBytes)

		list = append(list, networkDiagnosticEvidenceFile)
	}
	objectTypeResultsIterator.Close()

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of networkDiagnosticEvidenceFiles found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
