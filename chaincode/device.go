package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//Device definition of the structure
type Device struct {
	ObjectType           string
	Uuid                 string
	Name                 string
	PrettyName           string
	EUCitizenID          string
	Cpe                  string
	RiskLevel            string
	Provider             Organization
	DeviceClass          DeviceClass
	CurrentConfiguration Configuration
	CurrentPatch         Patch
}

func createDeviceOnLedger(
	stub shim.ChaincodeStubInterface, objectType string, uuid string, name string, prettyName string,
	euCitizen string, cpe string, riskLevel string, provider Organization, deviceClass DeviceClass,
	currentConfiguration Configuration, currentPatch Patch) []byte {

	device := Device{objectType, uuid, name, prettyName, euCitizen, cpe, riskLevel, provider,
		deviceClass, currentConfiguration, currentPatch}
	deviceAsJSONBytes := makeBytesFromDevice(stub, device)

	uuidIndexKey := createIndexKey(stub, device.Uuid, device.ObjectType)

	putEntityInLedger(stub, uuidIndexKey, deviceAsJSONBytes)
	return deviceAsJSONBytes
}

func makeDeviceFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) Device {
	device := Device{}
	err := json.Unmarshal(bytes, &device)
	panicErr(err)
	return device
}

func makeBytesFromDevice(stub shim.ChaincodeStubInterface, device Device) []byte {
	bytes, err := json.Marshal(device)
	panicErr(err)
	return bytes
}

//CreateDevice method to create a device
func (t *Device) CreateDevice(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n CreateDevice - Start")
	uuid := args[0]
	name := args[1]
	prettyName := args[2]
	euCitizen := args[3]
	cpe := args[4]
	riskLevel := args[5]
	provider := args[6]
	deviceClass := args[7]
	currentConfiguration := args[8]
	currentPatch := args[9]
	currentConfigurationObj := Configuration{}
	currentPatchObj := Patch{}

	deviceIndexKey := createIndexKey(stub, uuid, "device")
	if checkEntityExist(stub, deviceIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "device")
	}

	providerIndexKey := createIndexKey(stub, provider, "organization")
	if checkEntityExist(stub, providerIndexKey) == false {
		return entityNotFoundMessage(stub, provider, "organization")
	}
	providerAsBytes := getEntityFromLedger(stub, providerIndexKey)
	providerUUID := makeOrganizationFromBytes(stub, providerAsBytes)

	deviceClassIndexKey := createIndexKey(stub, deviceClass, "deviceclass")
	if checkEntityExist(stub, deviceClassIndexKey) == false {
		return entityNotFoundMessage(stub, deviceClass, "deviceclass")
	}
	deviceClassAsBytes := getEntityFromLedger(stub, deviceClassIndexKey)
	deviceClassUUID := makeDeviceClassFromBytes(stub, deviceClassAsBytes)

	if currentConfiguration != "" {
		currentConfigurationIndexKey := createIndexKey(stub, currentConfiguration, "configuration")
		if checkEntityExist(stub, currentConfigurationIndexKey) == false {
			return entityNotFoundMessage(stub, currentConfiguration, "configuration")
		}
		currentConfigurationAsBytes := getEntityFromLedger(stub, currentConfigurationIndexKey)
		currentConfigurationObj = makeConfigurationFromBytes(stub, currentConfigurationAsBytes)
	}
	if currentPatch != "" {
		currentPatchIndexKey := createIndexKey(stub, currentPatch, "patch")
		if checkEntityExist(stub, currentPatchIndexKey) == false {
			return entityNotFoundMessage(stub, currentPatch, "patch")
		}
		currentPatchAsBytes := getEntityFromLedger(stub, currentPatchIndexKey)
		currentPatchObj = makePatchFromBytes(stub, currentPatchAsBytes)
	}
	createdDeviceAsBytes := createDeviceOnLedger(stub, "device", uuid, name, prettyName, euCitizen, cpe, riskLevel,
		providerUUID, deviceClassUUID, currentConfigurationObj, currentPatchObj)

	return succeed(stub, "deviceCreatedEvent", createdDeviceAsBytes)
}

//GetDeviceByID method to get a device by id
func (t *Device) GetDeviceByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nGetDeviceByID - Start")

	uuid := args[0]
	uuidIndexKey := createIndexKey(stub, uuid, "device")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "device")
	}
	deviceAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	return shim.Success(deviceAsBytes)
}

//UpdateDeviceByID method to update an device by id
func (t *Device) UpdateDeviceByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n UpdateDeviceByID - Start")

	uuid := args[0]
	newName := args[1]
	newPrettyName := args[2]
	newEuCitizen := args[3]
	newCpe := args[4]
	newRiskLevel := args[5]
	newProvider := args[6]
	newDeviceClass := args[7]
	newConfiguration := args[8]
	newPatch := args[9]

	uuidIndexKey := createIndexKey(stub, uuid, "device")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "device")
	}
	deviceAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	device := makeDeviceFromBytes(stub, deviceAsBytes)

	device.Name = newName
	device.PrettyName = newPrettyName
	device.EUCitizenID = newEuCitizen
	device.Cpe = newCpe
	device.RiskLevel = newRiskLevel

	orgUUIDIndexKey := createIndexKey(stub, newProvider, "organization")
	if checkEntityExist(stub, orgUUIDIndexKey) == false {
		return entityNotFoundMessage(stub, newProvider, "organization")
	}
	providerAsBytes := getEntityFromLedger(stub, orgUUIDIndexKey)
	provider := makeOrganizationFromBytes(stub, providerAsBytes)
	device.Provider = provider

	deviceClassIndexKey := createIndexKey(stub, newDeviceClass, "deviceclass")
	if checkEntityExist(stub, deviceClassIndexKey) == false {
		return entityNotFoundMessage(stub, newDeviceClass, "deviceclass")
	}
	deviceClassAsBytes := getEntityFromLedger(stub, deviceClassIndexKey)
	deviceClass := makeDeviceClassFromBytes(stub, deviceClassAsBytes)
	device.DeviceClass = deviceClass

	currentConfigurationIndexKey := createIndexKey(stub, newConfiguration, "configuration")
	if checkEntityExist(stub, currentConfigurationIndexKey) == false {
		return entityNotFoundMessage(stub, newConfiguration, "configuration")
	}
	currentConfigurationAsBytes := checkEntityExistsAndGet(stub, string(currentConfigurationIndexKey))
	currentConfiguration := makeConfigurationFromBytes(stub, currentConfigurationAsBytes)
	device.CurrentConfiguration = currentConfiguration

	currentPatchIndexKey := createIndexKey(stub, newPatch, "patch")
	if checkEntityExist(stub, currentPatchIndexKey) == false {
		return entityNotFoundMessage(stub, newPatch, "patch")
	}
	currentPatchAsBytes := getEntityFromLedger(stub, string(currentPatchIndexKey))
	currentPatch := makePatchFromBytes(stub, currentPatchAsBytes)
	device.CurrentPatch = currentPatch

	orgAsJSONBytes := makeBytesFromDevice(stub, device)

	putEntityInLedger(stub, uuidIndexKey, orgAsJSONBytes)
	return succeed(stub, "deviceUpdatedEvent", orgAsJSONBytes)

}

//UnregisterDeviceByID method to unregister device by id
func (t *Device) UnregisterDeviceByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n UnregisterDeviceByID - Start")
	uuid := args[0]

	uuidIndexKey := createIndexKey(stub, uuid, "device")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "device")
	}
	deviceAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	makeDeviceFromBytes(stub, deviceAsBytes)

	deleteEntityFromLedger(stub, uuidIndexKey)
	fmt.Println("Device " + uuid + " was successfully unregistered")
	return succeed(stub, "deviceUnregisteredEvent", []byte("{\"uuid\":\""+uuid+"\"}"))
}

//GetAllDevices method to get and list all devices from ledger
func (t *Device) GetAllDevices(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllDevices - Start:(%s)\n", args)

	objectType := args[0]
	device := Device{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []Device
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		deviceAsBytes := getEntityFromLedger(stub, responseRange.Key)
		device = makeDeviceFromBytes(stub, deviceAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, deviceAsBytes)

		list = append(list, device)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of devices found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllDevicesByPage method to get and list all devices from ledger by page
func (t *Device) GetAllDevicesByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("GetAllDevicesByPage - Start:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"device"})
	panicErr(err)

	var list []Device

	for i := 0; i < offset && objectTypeResultsIterator.HasNext(); i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		deviceAsBytes := getEntityFromLedger(stub, responseRange.Key)
		device := makeDeviceFromBytes(stub, deviceAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, deviceAsBytes)

		list = append(list, device)
	}
	objectTypeResultsIterator.Close()

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of devices found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllDeviceByDeviceClass method to get and list all devices from ledger by DeviceClass
func (t *Device) GetAllDeviceByDeviceClass(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllDeviceByDeviceClass - Start:(%s)\n", args)

	objectType := args[0]
	deviceClassUUID := args[1]
	device := Device{}

	uuidIndexKey := createIndexKey(stub, deviceClassUUID, "deviceclass")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, deviceClassUUID, "deviceclass")
	}

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []Device
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		deviceClassAsBytes := getEntityFromLedger(stub, responseRange.Key)
		device = makeDeviceFromBytes(stub, deviceClassAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, deviceClassAsBytes)
		if device.DeviceClass.Uuid == deviceClassUUID {
			list = append(list, device)
		}
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of configurationsfile found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllDeviceByOrganization method to get and list all devices from ledger by Organization
func (t *Device) GetAllDeviceByOrganization(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllDeviceByOrganization - Start:(%s)\n", args)

	objectType := args[0]
	organizationUUID := args[1]
	device := Device{}

	uuidIndexKey := createIndexKey(stub, organizationUUID, "organization")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, organizationUUID, "organization")
	}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []Device
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		deviceAsBytes := getEntityFromLedger(stub, responseRange.Key)
		device = makeDeviceFromBytes(stub, deviceAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, deviceAsBytes)
		if device.Provider.Uuid == organizationUUID {
			list = append(list, device)
		}
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of configurationsfile found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetDevicesOwnedByEuCitizen method to get and list all devices from ledger by EuCitizen
func (t *Device) GetDevicesOwnedByEuCitizen(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetDevicesOwnedByEuCitizen - Start:(%s)\n", args)

	objectType := args[0]
	eucitizen := args[1]
	device := Device{}

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []Device
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		deviceAsBytes := getEntityFromLedger(stub, responseRange.Key)
		device = makeDeviceFromBytes(stub, deviceAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, deviceAsBytes)
		if device.EUCitizenID == eucitizen {
			list = append(list, device)
		}
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of Device found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
