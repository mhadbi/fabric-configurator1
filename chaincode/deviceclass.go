package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//DeviceClass declaration of struct
type DeviceClass struct {
	ObjectType   string
	Uuid         string
	Name         string
	PrettyName   string
	Manufacturer Organization
}

func createDeviceClassOnLedger(stub shim.ChaincodeStubInterface, uuid string, name string, prettyName string,
	manufacturer Organization) []byte {
	deviceClass := DeviceClass{"deviceclass", uuid, name, prettyName, manufacturer}
	dcAsJSONBytes := makeBytesFromDeviceClass(stub, deviceClass)

	uuidIndexKey := createIndexKey(stub, deviceClass.Uuid, "deviceclass")

	putEntityInLedger(stub, uuidIndexKey, dcAsJSONBytes)
	return dcAsJSONBytes
}

func makeDeviceClassFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) DeviceClass {
	dc := DeviceClass{}
	err := json.Unmarshal(bytes, &dc)
	panicErr(err)
	return dc
}

func makeBytesFromDeviceClass(stub shim.ChaincodeStubInterface, deviceClass DeviceClass) []byte {
	bytes, err := json.Marshal(deviceClass)
	panicErr(err)
	return bytes
}

//CreateDeviceClass method to create deviceClass
func (t *DeviceClass) CreateDeviceClass(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n CreateDeviceClass - Start")

	uuid := args[0]
	name := args[1]
	prettyName := args[2]
	manufacturerUUID := args[3]

	deviceClassIndexKey := createIndexKey(stub, uuid, "deviceclass")
	if checkEntityExist(stub, deviceClassIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "deviceclass")
	}

	uuidIndexKey := createIndexKey(stub, manufacturerUUID, "organization")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, manufacturerUUID, "organization")
	}
	manufacturerAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	manufacturer := makeOrganizationFromBytes(stub, manufacturerAsBytes)

	createdDeviceClass := createDeviceClassOnLedger(stub, uuid, name, prettyName, manufacturer)
	return succeed(stub, "deviceClassCreatedEvent", createdDeviceClass)
}

//GetDeviceClassByID method to get device class from ledger
func (t *DeviceClass) GetDeviceClassByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nGetDeviceClassByID - Start")
	uuid := args[0]

	uuidIndexKey := createIndexKey(stub, uuid, "deviceclass")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "deviceclass")
	}
	deviceClassAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	return shim.Success(deviceClassAsBytes)
}

//UpdateDeviceClassByID method to update an deviceClass by id
func (t *DeviceClass) UpdateDeviceClassByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n UpdateDeviceClassByID - Start")

	uuid := args[0]
	newName := args[1]
	newPrettyName := args[2]
	newManufacturerUUID := args[3]

	uuidIndexKey := createIndexKey(stub, uuid, "deviceclass")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "deviceclass")
	}
	deviceClassAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	deviceClass := makeDeviceClassFromBytes(stub, deviceClassAsBytes)

	deviceClass.Name = newName
	deviceClass.PrettyName = newPrettyName

	orgUUIDIndexKey := createIndexKey(stub, newManufacturerUUID, "organization")
	if checkEntityExist(stub, orgUUIDIndexKey) == false {
		return entityNotFoundMessage(stub, newManufacturerUUID, "organization")
	}
	manufacturerAsBytes := getEntityFromLedger(stub, string(orgUUIDIndexKey))
	manufacturer := makeOrganizationFromBytes(stub, manufacturerAsBytes)

	deviceClass.Manufacturer = manufacturer

	deviceClassAsJSONBytes := makeBytesFromDeviceClass(stub, deviceClass)

	putEntityInLedger(stub, uuidIndexKey, deviceClassAsJSONBytes)
	return succeed(stub, "deviceClassUpdatedEvent", deviceClassAsJSONBytes)

}

//UnregisterConfigurationByDeviceClass method to unregister Configuration By DeviceClass class from ledger
func UnregisterConfigurationByDeviceClass(stub shim.ChaincodeStubInterface, deviceClass string) pb.Response {
	fmt.Println("\n UnregisterConfigurationByDeviceClass - Start")

	configuration := Configuration{}
	objectTypeResultsIteratorConf, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"configuration"})
	if err != nil {
		return shim.Error(err.Error())
	}
	var i int
	for i = 0; objectTypeResultsIteratorConf.HasNext() == true; i++ {
		fmt.Println("\n UnregisterConfiguration - Start")
		responseRange, err := objectTypeResultsIteratorConf.Next()
		panicErr(err)
		configurationAsBytes := getEntityFromLedger(stub, responseRange.Key)
		configuration = makeConfigurationFromBytes(stub, configurationAsBytes)

		if configuration.DeviceClass.Uuid == deviceClass {

			UnregisterConfigurationFileByConfiguration(stub, configuration.Uuid)

			configurationUUIDIndexKey := createIndexKey(stub, configuration.Uuid, "configuration")
			deleteEntityFromLedger(stub, configurationUUIDIndexKey)
			fmt.Println("Configuration " + configuration.Uuid + " was unregistered successfully")
		}
	}
	return shim.Success([]byte(""))
}

//UnregisterPatchByDeviceClass method to unregister Patch By DeviceClass class from ledger
func UnregisterPatchByDeviceClass(stub shim.ChaincodeStubInterface, deviceClass string) pb.Response {
	fmt.Println("\n UnregisterPatchByDeviceClass - Start")

	patch := Patch{}
	objectTypeResultsIteratorPatch, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"patch"})
	if err != nil {
		return shim.Error(err.Error())
	}
	var ii int
	for ii = 0; objectTypeResultsIteratorPatch.HasNext() == true; ii++ {
		fmt.Println("\n UnregisterPatch - Start")
		responseRange, err := objectTypeResultsIteratorPatch.Next()
		panicErr(err)
		patchAsBytes := getEntityFromLedger(stub, responseRange.Key)
		patch = makePatchFromBytes(stub, patchAsBytes)

		if patch.DeviceClass.Uuid == deviceClass {

			UnregisterPatchFileByPatch(stub, patch.Uuid)

			patchUUIDIndexKey := createIndexKey(stub, patch.Uuid, "patch")
			deleteEntityFromLedger(stub, patchUUIDIndexKey)
			fmt.Println("Patch " + patch.Uuid + " was unregistered successfully")
		}
	}
	return shim.Success([]byte(""))
}

//UnregisterDeviceClassByID method to unregister device class from ledger
func (t *DeviceClass) UnregisterDeviceClassByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nUnregisterDeviceClassByID - Start")
	uuid := args[0]
	uuidIndexKey := createIndexKey(stub, uuid, "deviceclass")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "deviceclass")
	}

	//delete all configuration  by this configuration
	UnregisterConfigurationByDeviceClass(stub, uuid)

	//delete all patch by this patch
	UnregisterPatchByDeviceClass(stub, uuid)

	//delete Deviceclass
	deleteEntityFromLedger(stub, uuidIndexKey)

	fmt.Println("Device Class " + uuid + " was unregistered successfully")
	return succeed(stub, "deviceClassUnregisteredEvent", []byte("{\"DeviceClassUUID\":\""+uuid+"\"}"))
}

//UpdateCurrentConfiguration method to update current configuration
func UpdateCurrentConfiguration(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nUpdateCurrentConfiguration - Start")
	configurationUUID := args[0]
	deviceClass := args[1]

	checkEntityExistsAndGet(stub, configurationUUID)

	deviceClassAsBytes := checkEntityExistsAndGet(stub, deviceClass)
	deviceClassUUID := makeDeviceClassFromBytes(stub, deviceClassAsBytes)

	newDCAsBytes := makeBytesFromDeviceClass(stub, deviceClassUUID)
	putEntityInLedger(stub, deviceClass, newDCAsBytes)

	return succeed(stub, "currentConfigurationAddedToDeviceClassEvent", newDCAsBytes)
}

//GetAllDeviceClass method to get all deviceclass from ledger
func (t *DeviceClass) GetAllDeviceClass(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllDeviceClasses:(%s)\n", args)
	objectType := args[0]

	deviceClass := DeviceClass{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []DeviceClass
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		deviceclassAsBytes := getEntityFromLedger(stub, responseRange.Key)
		deviceClass = makeDeviceClassFromBytes(stub, deviceclassAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, deviceclassAsBytes)

		list = append(list, deviceClass)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of deviceclass found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllDeviceClassWithPage method to get all device from ledger by page
func (t *DeviceClass) GetAllDeviceClassWithPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllDeviceClassByPages:(%s)\n", args)
	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"deviceclass"})
	panicErr(err)

	var list []DeviceClass

	for i := 0; i < offset && objectTypeResultsIterator.HasNext() == true; i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		deviceclassAsBytes := getEntityFromLedger(stub, responseRange.Key)
		deviceclass := makeDeviceClassFromBytes(stub, deviceclassAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, deviceclassAsBytes)

		list = append(list, deviceclass)
	}
	objectTypeResultsIterator.Close()

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of organizations found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllDeviceClassByOrganization method to get all deviceClass by organization
func (t *DeviceClass) GetAllDeviceClassByOrganization(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllAdministratorsByOrganization - Start:(%s)\n", args)

	objectType := args[0]
	organanization := args[1]
	deviceClass := DeviceClass{}
	uuidIndexKey := createIndexKey(stub, organanization, "organization")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, organanization, "organization")
	}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []DeviceClass
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		deviceClassAsBytes := getEntityFromLedger(stub, responseRange.Key)
		deviceClass = makeDeviceClassFromBytes(stub, deviceClassAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, deviceClassAsBytes)
		if deviceClass.Manufacturer.Uuid == organanization {
			list = append(list, deviceClass)
		}

	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of deviceCLass By Organization found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
