package main

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkCreateAdministratorWhenOrganizationExists(t *testing.T, stub *shim.MockStub, employeeID string, parentOrganization string, accreditationLevel string) {
	displayNewTest("Create Administrator when Parent Organization Exists")

	// Request
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateAdministrator"), []byte(employeeID), []byte(parentOrganization), []byte(accreditationLevel)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkCreateAdministratorWhenOrganizationDoesNotExist(t *testing.T, stub *shim.MockStub, employeeID string, parentOrganization string, accreditationLevel string) {
	displayNewTest("Create Administrator when Parent Organization DOES NOT Exist")

	// Request
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateAdministrator"), []byte(employeeID), []byte(parentOrganization), []byte(accreditationLevel)})

	if response.Status == shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkGetExistingAdministrator(t *testing.T, stub *shim.MockStub, employeeID string) {
	displayNewTest("Get Existing Administrator " + employeeID + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAdministratorByEmployeeID"), []byte(employeeID)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	admin := Administrator{}
	_ = json.Unmarshal(response.Payload, &admin)

	if admin.EmployeeID != employeeID {
		t.Fail()
	}
}

func checkGetNonExistingAdministrator(t *testing.T, stub *shim.MockStub, employeeID string) {
	displayNewTest("Get NON Existing Administrator " + employeeID + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAdministratorByEmployeeID"), []byte(employeeID)})
	if response.Status != shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkUnregisterAdministrator(t *testing.T, stub *shim.MockStub, employeeID string) {
	displayNewTest("Unregister existing administrator " + employeeID + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("UnregisterAdministratorByEmployeeID"), []byte(employeeID)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllAdministrators(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType Administrator ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllAdministrators"),
		[]byte(objectType)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllAdministratorByOrganization(t *testing.T, stub *shim.MockStub, objectType string, organizationUUID string) {
	displayNewTest("GetAllByObjectType Administrator  By Organization")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllAdministratorsByOrganization"),
		[]byte(objectType), []byte(organizationUUID)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllAdministratorsByPage(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType Administrator By Page ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllAdministratorsByPage"), []byte("4"), []byte("0")})

	response = stub.MockInvoke("1", [][]byte{[]byte("GetAllAdministratorsByPage"), []byte("2"), []byte("3")})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestAdministrator(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O1"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A1", "O1", "1")
	checkCreateAdministratorWhenOrganizationDoesNotExist(t, stub, "A1", "O1", "1")

	checkGetExistingAdministrator(t, stub, "A1")

}

func TestCreateAdministrator(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O1"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})

	checkCreateAdministratorWhenOrganizationExists(t, stub, "A1", "O1", "1")
	checkCreateAdministratorWhenOrganizationDoesNotExist(t, stub, "A2", "O2", "1")

	checkGetExistingAdministrator(t, stub, "A1")
}

func TestGetAllAdministratorsByPage(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O1"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})

	checkCreateAdministratorWhenOrganizationExists(t, stub, "A1", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A2", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A3", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A4", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A5", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A6", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A7", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A8", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A9", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A10", "O1", "1")

	checkGetAllAdministratorsByPage(t, stub, "administrator")
}
func TestGetAllAdministrators(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	checkCreateNewOrganization(t, stub, "O1", "SCHAIN", "Scorechain", "ISP")

	checkCreateAdministratorWhenOrganizationExists(t, stub, "A1", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A2", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A3", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A4", "O1", "1")

	checkGetAllAdministrators(t, stub, "administrator")
}

func TestCheckGetAllAdministratorByOrganization(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	checkCreateNewOrganization(t, stub, "O1", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O2", "SCHAIN", "Scorechain", "ISP")

	checkCreateAdministratorWhenOrganizationExists(t, stub, "A1", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A2", "O2", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A3", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A4", "O2", "1")

	checkGetAllAdministratorByOrganization(t, stub, "administrator", "O2")
}

func TestCheckUnregisterAdministrator(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O1"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A1", "O1", "1")

	checkUnregisterAdministrator(t, stub, "A1")
}
