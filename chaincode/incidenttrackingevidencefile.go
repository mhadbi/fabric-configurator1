package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type IncidentTrackingEvidenceFile struct {
	ObjectType    string
	SubObjectType string
	Uuid          string
	EvidenceFile  EvidenceFile
}

func createIncidentTrackingEvidenceFileOnLedger(stub shim.ChaincodeStubInterface, objectType string, uuid string, name string, prettyName string,
	evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string, attackStatus string, evidence Evidence) []byte {

	evidenceFile := EvidenceFile{name, prettyName, evidenceFileDataSourceTitle,
		evidenceFileInformation, evidenceFileCreationDate, attackStatus, evidence}

	incidentTrackingEvidenceFile := IncidentTrackingEvidenceFile{objectType, "incidentTrackingEvidenceFile", uuid, evidenceFile}
	incidentTrackingEvidenceFileAsJSONBytes := makeBytesFromIncidentTrackingEvidenceFile(stub, incidentTrackingEvidenceFile)
	uuidIndexKey2 := createIndexKey2(stub, []string{"evidenceFile", "incidentTrackingEvidenceFile", uuid})
	putEntityInLedger(stub, uuidIndexKey2, incidentTrackingEvidenceFileAsJSONBytes)

	return incidentTrackingEvidenceFileAsJSONBytes
}

func makeIncidentTrackingEvidenceFileFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) IncidentTrackingEvidenceFile {
	incidentTrackingEvidenceFile := IncidentTrackingEvidenceFile{}
	err := json.Unmarshal(bytes, &incidentTrackingEvidenceFile)
	panicErr(err)
	return incidentTrackingEvidenceFile
}

func makeBytesFromIncidentTrackingEvidenceFile(stub shim.ChaincodeStubInterface, incidentTrackingEvidenceFile IncidentTrackingEvidenceFile) []byte {
	bytes, err := json.Marshal(incidentTrackingEvidenceFile)
	panicErr(err)
	return bytes
}

func makeBytesFromEvidenceFile(stub shim.ChaincodeStubInterface, evidenceFile EvidenceFile) []byte {
	bytes, err := json.Marshal(evidenceFile)
	panicErr(err)
	return bytes
}

//CreateIncidentTrackingEvidenceFile method to create an incidentTrackingEvidenceFile
func (t *IncidentTrackingEvidenceFile) CreateIncidentTrackingEvidenceFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n CreateIncidentTrackingEvidenceFile - Start")
	uuid := args[0]
	name := args[1]
	prettyName := args[2]
	evidenceFileDataSourceTitle := args[3]
	evidenceFileInformation := args[4]
	evidenceFileCreationDate := args[5]
	attackStatus := args[6]
	evidence := args[7]

	uuidIndexKey := createIndexKey2(stub, []string{"evidenceFile", "incidentTrackingEvidenceFile", uuid})
	if checkEntityExist(stub, uuidIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "evidenceFile")
	}
	evidenceIndexKey := createIndexKey(stub, evidence, "evidence")
	if checkEntityExist(stub, evidenceIndexKey) == false {
		return entityNotFoundMessage(stub, evidence, "evidence")
	}
	evidenceAsBytes := getEntityFromLedger(stub, evidenceIndexKey)
	evidenceUUID := makeEvidenceFromBytes(stub, evidenceAsBytes)

	createdIncidentTrackingEvidenceFile := createIncidentTrackingEvidenceFileOnLedger(stub, "evidenceFile", uuid, name,
		prettyName, evidenceFileDataSourceTitle, evidenceFileInformation, evidenceFileCreationDate, attackStatus, evidenceUUID)
	return succeed(stub, "incidentTrackingEvidenceFileCreatedEvent", createdIncidentTrackingEvidenceFile)
}

//GetIncidentTrackingEvidenceFileByID method to get an incidentTrackingEvidenceFile by id
func (t *IncidentTrackingEvidenceFile) GetIncidentTrackingEvidenceFileByID(stub shim.ChaincodeStubInterface, args string) pb.Response {
	fmt.Println("\n GetIncidentTrackingEvidenceFileByID - Start", args)

	uuid := args

	uuidIndexKey := createIndexKey2(stub, []string{"evidenceFile", "incidentTrackingEvidenceFile", uuid})
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "evidenceFile")
	}
	incidentTrackingEvidenceFileAsBytes := getEntityFromLedger(stub, uuidIndexKey)

	return shim.Success(incidentTrackingEvidenceFileAsBytes)
}

//GetAllIncidentTrackingEvidenceFile Method to get all incidentTrackingEvidenceFiles from the ledger
func (t *IncidentTrackingEvidenceFile) GetAllIncidentTrackingEvidenceFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllIncidentTrackingEvidenceFile:(%s)\n", args)
	objectType := args[0]
	incidentTrackingEvidenceFile := IncidentTrackingEvidenceFile{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~subObjectType~uuid", []string{objectType, "incidentTrackingEvidenceFile"})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []IncidentTrackingEvidenceFile
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		incidentTrackingEvidenceFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		incidentTrackingEvidenceFile = makeIncidentTrackingEvidenceFileFromBytes(stub, incidentTrackingEvidenceFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, incidentTrackingEvidenceFileAsBytes)

		list = append(list, incidentTrackingEvidenceFile)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of incidentTrackingEvidenceFiles found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllIncidentTrackingEvidenceFileByPage Method to get all incidentTrackingEvidenceFiles from the ledger
func (t *IncidentTrackingEvidenceFile) GetAllIncidentTrackingEvidenceFileByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllIncidentTrackingEvidenceFilesByPage:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~subObjectType~uuid", []string{"evidenceFile", "incidentTrackingEvidenceFile"})
	panicErr(err)

	var list []IncidentTrackingEvidenceFile

	for i := 0; i < offset && objectTypeResultsIterator.HasNext() == true; i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		incidentTrackingEvidenceFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		incidentTrackingEvidenceFile := makeIncidentTrackingEvidenceFileFromBytes(stub, incidentTrackingEvidenceFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, incidentTrackingEvidenceFileAsBytes)

		list = append(list, incidentTrackingEvidenceFile)
	}
	objectTypeResultsIterator.Close()

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of incidentTrackingEvidenceFiles found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
