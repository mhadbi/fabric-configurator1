package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type OSEvidenceFile struct {
	ObjectType    string
	SubObjectType string
	Uuid          string
	OSName        string
	OSVersion     string
	EvidenceFile  EvidenceFile
	DeviceLogs    []string
}

func createOSEvidenceFileOnLedger(stub shim.ChaincodeStubInterface, objectType string, uuid string, name string, prettyName string,
	evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string, attackStatus string, osName string, osVersion string, evidence Evidence, deviceLogs []string) []byte {

	evidenceFile := EvidenceFile{name, prettyName, evidenceFileDataSourceTitle,
		evidenceFileInformation, evidenceFileCreationDate, attackStatus, evidence}

	osEvidenceFile := OSEvidenceFile{objectType, "osEvidenceFile", uuid, osName, osVersion, evidenceFile, deviceLogs}
	osEvidenceFileAsJSONBytes := makeBytesFromOSEvidenceFile(stub, osEvidenceFile)
	uuidIndexKey2 := createIndexKey2(stub, []string{objectType, "osEvidenceFile", uuid})
	putEntityInLedger(stub, uuidIndexKey2, osEvidenceFileAsJSONBytes)

	return osEvidenceFileAsJSONBytes
}

func makeOSEvidenceFileFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) OSEvidenceFile {
	osEvidenceFile := OSEvidenceFile{}
	err := json.Unmarshal(bytes, &osEvidenceFile)
	panicErr(err)
	return osEvidenceFile
}

func makeBytesFromOSEvidenceFile(stub shim.ChaincodeStubInterface, osEvidenceFile OSEvidenceFile) []byte {
	bytes, err := json.Marshal(osEvidenceFile)
	panicErr(err)
	return bytes
}

//CreateOSEvidenceFile method to create an osEvidenceFile
func (t *OSEvidenceFile) CreateOSEvidenceFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n CreateOSEvidenceFile - Start")
	uuid := args[0]
	name := args[1]
	prettyName := args[2]
	evidenceFileDataSourceTitle := args[3]
	evidenceFileInformation := args[4]
	evidenceFileCreationDate := args[5]
	attackStatus := args[6]
	osName := args[7]
	osVersion := args[8]
	evidence := args[9]
	deviceLogsString := args[10]

	uuidIndexKey := createIndexKey2(stub, []string{"evidenceFile", "osEvidenceFile", uuid})
	if checkEntityExist(stub, uuidIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "evidenceFile")
	}
	evidenceIndexKey := createIndexKey(stub, evidence, "evidence")
	if checkEntityExist(stub, evidenceIndexKey) == false {
		return entityNotFoundMessage(stub, evidence, "evidence")
	}

	deviceLogs := strings.Fields(deviceLogsString)

	for i := range deviceLogs {
		loguuidIndexKey := createIndexKey(stub, deviceLogs[i], "devicelog")
		if checkEntityExist(stub, loguuidIndexKey) == false {
			return entityNotFoundMessage(stub, deviceLogs[i], "devicelog")
		}
	}

	evidenceAsBytes := getEntityFromLedger(stub, evidenceIndexKey)
	evidenceUUID := makeEvidenceFromBytes(stub, evidenceAsBytes)

	createdOSEvidenceFile := createOSEvidenceFileOnLedger(stub, "evidenceFile", uuid, name,
		prettyName, evidenceFileDataSourceTitle, evidenceFileInformation, evidenceFileCreationDate, attackStatus, osName, osVersion, evidenceUUID, deviceLogs)
	return succeed(stub, "osEvidenceFileCreatedEvent", createdOSEvidenceFile)
}

//GetOSEvidenceFileByID method to get an osEvidenceFile by id
func (t *OSEvidenceFile) GetOSEvidenceFileByID(stub shim.ChaincodeStubInterface, args string) pb.Response {
	fmt.Println("\n GetOSEvidenceFileByID - Start", args)

	uuid := args

	uuidIndexKey := createIndexKey2(stub, []string{"evidenceFile", "osEvidenceFile", uuid})
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "evidenceFile")
	}
	osEvidenceFileAsBytes := getEntityFromLedger(stub, uuidIndexKey)

	return shim.Success(osEvidenceFileAsBytes)
}

//GetAllOSEvidenceFile Method to get all osEvidenceFiles from the ledger
func (t *OSEvidenceFile) GetAllOSEvidenceFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllOSEvidenceFile:(%s)\n", args)
	objectType := args[0]
	osEvidenceFile := OSEvidenceFile{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~subObjectType~uuid", []string{objectType, "osEvidenceFile"})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []OSEvidenceFile
	fmt.Printf("-Found list of %s\n ", objectTypeResultsIterator)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		osEvidenceFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		osEvidenceFile = makeOSEvidenceFileFromBytes(stub, osEvidenceFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, osEvidenceFileAsBytes)

		list = append(list, osEvidenceFile)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of osEvidenceFiles found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllOSEvidenceFileByPage Method to get all osEvidenceFiles from the ledger
func (t *OSEvidenceFile) GetAllOSEvidenceFileByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllOSEvidenceFilesByPage:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~subObjectType~uuid", []string{"evidenceFile", "osEvidenceFile"})
	panicErr(err)

	var list []OSEvidenceFile

	for i := 0; i < offset && objectTypeResultsIterator.HasNext() == true; i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		osEvidenceFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		osEvidenceFile := makeOSEvidenceFileFromBytes(stub, osEvidenceFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, osEvidenceFileAsBytes)

		list = append(list, osEvidenceFile)
	}
	objectTypeResultsIterator.Close()

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of osEvidenceFiles found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
