package main

import (
	"encoding/json"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkGetExistingSoftwareEvidenceFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Existing SoftwareEvidenceFile " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetSoftwareEvidenceFileByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	dc := SoftwareEvidenceFile{}
	_ = json.Unmarshal(response.Payload, &dc)

	if dc.Uuid != uuid {
		t.Fail()
	}
}

func checkGetNonExistingSoftwareEvidenceFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get NON Existing SoftwareEvidenceFile " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetSoftwareEvidenceFileByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkCreateSoftwareEvidenceFileWhenEvidenceDoesNotExist(t *testing.T, stub *shim.MockStub, uuid string,
	name string, prettyName string, evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string,
	attackStatus string, softwareName string, softwareVersion string, softwareResult string, evidence string, deviceLogs []string) {
	displayNewTest("Create SoftwareEvidenceFile when Evidence Org DOES NOT Exist")
	deviceLogsString := strings.Join(deviceLogs, ",")
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateSoftwareEvidenceFile"), []byte(uuid), []byte(name), []byte(prettyName), []byte(evidenceFileDataSourceTitle),
		[]byte(evidenceFileInformation), []byte(evidenceFileCreationDate), []byte(attackStatus), []byte(softwareName), []byte(softwareVersion), []byte(softwareResult), []byte(evidence), []byte(deviceLogsString)})
	if response.Status == shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkCreateSoftwareEvidenceFileWhenEvidenceExist(t *testing.T, stub *shim.MockStub, uuid string,
	name string, prettyName string, evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string,
	attackStatus string, softwareName string, softwareVersion string, softwareResult string, evidence string, deviceLogs []string) {
	displayNewTest("Create SoftwareEvidenceFile when Evidence Exist")
	deviceLogsString := strings.Join(deviceLogs, ",")
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateSoftwareEvidenceFile"), []byte(uuid), []byte(name), []byte(prettyName), []byte(evidenceFileDataSourceTitle),
		[]byte(evidenceFileInformation), []byte(evidenceFileCreationDate), []byte(attackStatus), []byte(softwareName), []byte(softwareVersion), []byte(softwareResult), []byte(evidence), []byte(deviceLogsString)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllSoftwareEvidenceFile(t *testing.T, stub *shim.MockStub, objectType string, numberOfDeviceCreated int) {
	displayNewTest("GetAllByObjectType SoftwareEvidenceFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllSoftwareEvidenceFile"),
		[]byte(objectType)})

	var liste []SoftwareEvidenceFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil || len(liste) != numberOfDeviceCreated {
		t.Fail()
	}
}

func checkGetAllSoftwareEvidenceFileWithPage(t *testing.T, stub *shim.MockStub, pageSize string, offset string) {
	displayNewTest("GetAllByObjectTypeWithpage SoftwareEvidenceFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllSoftwareEvidenceFileByPage"), []byte(pageSize), []byte(offset)})
	var liste []SoftwareEvidenceFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestCreateSoftwareEvidenceFileWhenEvidenceExist(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	mockTest(t, stub)
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte("DL1"), []byte("name"), []byte("typeOfDeviceLog"), []byte("format"), []byte("dateStart"),
		[]byte("dateEnd"), []byte("dateOfficial"), []byte("storageURL"), []byte("fileHash"), []byte("D1")})

	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateSoftwareEvidenceFileWhenEvidenceExist(t, stub, "NDE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E0", []string{"DL1"})
}

func TestCreateSoftwareEvidenceFileWhenEvidenceDoesNotExist(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	mockTest(t, stub)
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte("DL1"), []byte("name"), []byte("typeOfDeviceLog"), []byte("format"), []byte("dateStart"),
		[]byte("dateEnd"), []byte("dateOfficial"), []byte("storageURL"), []byte("fileHash"), []byte("D1")})

	checkCreateSoftwareEvidenceFileWhenEvidenceDoesNotExist(t, stub, "NDE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E0", []string{"DL1"})
}

func TestGetExistingSoftwareEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	mockTest(t, stub)
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte("DL1"), []byte("name"), []byte("typeOfDeviceLog"), []byte("format"), []byte("dateStart"),
		[]byte("dateEnd"), []byte("dateOfficial"), []byte("storageURL"), []byte("fileHash"), []byte("D1")})

	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateSoftwareEvidenceFileWhenEvidenceExist(t, stub, "NDE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E0", []string{"DL1"})

	checkGetExistingSoftwareEvidenceFile(t, stub, "NDE0")

}

func TestGetNonExistingSoftwareEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	checkGetNonExistingSoftwareEvidenceFile(t, stub, "NDE0")
}

func TestGetAllSoftwareEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	mockTest(t, stub)
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte("DL1"), []byte("name"), []byte("typeOfDeviceLog"), []byte("format"), []byte("dateStart"),
		[]byte("dateEnd"), []byte("dateOfficial"), []byte("storageURL"), []byte("fileHash"), []byte("D1")})

	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateSoftwareEvidenceFileWhenEvidenceExist(t, stub, "NDE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E0", []string{"DL1"})
	checkCreateSoftwareEvidenceFileWhenEvidenceExist(t, stub, "NDE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E0", []string{"DL1"})

	checkGetAllSoftwareEvidenceFile(t, stub, "softwareEvidenceFile", 2)
}

func TestGetAllSoftwareEvidenceFileWithPage(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	mockTest(t, stub)
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte("DL1"), []byte("name"), []byte("typeOfDeviceLog"), []byte("format"), []byte("dateStart"),
		[]byte("dateEnd"), []byte("dateOfficial"), []byte("storageURL"), []byte("fileHash"), []byte("D1")})

	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E1"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateSoftwareEvidenceFileWhenEvidenceExist(t, stub, "NDE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E0", []string{"DL1"})
	checkCreateSoftwareEvidenceFileWhenEvidenceExist(t, stub, "NDE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E0", []string{"DL1"})
	checkCreateSoftwareEvidenceFileWhenEvidenceExist(t, stub, "IE2", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E1", []string{"DL1"})
	checkCreateSoftwareEvidenceFileWhenEvidenceExist(t, stub, "IE3", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E1", []string{"DL1"})

	checkGetAllSoftwareEvidenceFileWithPage(t, stub, "100", "0")
}
