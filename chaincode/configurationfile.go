package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//ConfigurationFile struct definition
type ConfigurationFile struct {
	ObjectType        string
	Uuid              string
	ReleaseDate       string
	Status            string
	FileURL           string
	FileHash          string
	Version           string
	ConfigurationUuid Configuration
}

func createConfigurationFileOnLedger(stub shim.ChaincodeStubInterface, uuid string, releaseDate string,
	status string, fileURL string, fileHash string, version string, configurationUuid Configuration) []byte {
	configurationFile := ConfigurationFile{"configurationfile", uuid, releaseDate, status, fileURL,
		fileHash, version, configurationUuid}
	configurationFileAsBytes := makeBytesFromConfigurationFile(stub, configurationFile)

	uuidIndexKey := createIndexKey(stub, configurationFile.Uuid, "configurationfile")
	putEntityInLedger(stub, uuidIndexKey, configurationFileAsBytes)
	return configurationFileAsBytes
}

func makeConfigurationFileFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) ConfigurationFile {
	configurationFile := ConfigurationFile{}
	err := json.Unmarshal(bytes, &configurationFile)
	panicErr(err)
	return configurationFile
}

func makeBytesFromConfigurationFile(stub shim.ChaincodeStubInterface, configurationFile ConfigurationFile) []byte {
	bytes, err := json.Marshal(configurationFile)
	panicErr(err)
	return bytes
}

//CreateConfigurationFile method to get configurationfile
func (t *ConfigurationFile) CreateConfigurationFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nCreateConfigurationFile - Start")

	uuid := args[0]
	releaseDate := args[1]
	status := args[2]
	fileURL := args[3]
	fileHash := args[4]
	version := args[5]
	configurationUUID := args[6]

	configurationFileIndexKey := createIndexKey(stub, uuid, "configurationfile")
	if checkEntityExist(stub, configurationFileIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "configuration")
	}

	configurationUUIDIndexKey := createIndexKey(stub, configurationUUID, "configuration")
	if checkEntityExist(stub, configurationUUIDIndexKey) == false {
		return entityNotFoundMessage(stub, configurationUUID, "configuration")
	}
	configurationAsBytes := getEntityFromLedger(stub, configurationUUIDIndexKey)
	configuration := makeConfigurationFromBytes(stub, configurationAsBytes)

	configurationFileAsBytes := createConfigurationFileOnLedger(stub, uuid, releaseDate, status, fileURL, fileHash, version, configuration)
	return succeed(stub, "configurationFileCreatedEvent", configurationFileAsBytes)
}

//GetConfigurationFileByID method to get configurationfile by id
func (t *ConfigurationFile) GetConfigurationFileByID(stub shim.ChaincodeStubInterface, args string) pb.Response {
	fmt.Println("\nGetConfigurationFileByID - Start")

	uuid := args
	uuidIndexKey := createIndexKey(stub, uuid, "configurationfile")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "configurationfile")
	}
	configurationFileAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	return shim.Success(configurationFileAsBytes)
}

//UpdateConfigurationFileByID method to update an configurationFile by id
func (t *ConfigurationFile) UpdateConfigurationFileByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n UpdateConfigurationFileByID - Start")

	uuid := args[0]
	newReleaseDate := args[1]
	newStatus := args[2]
	newFileURL := args[3]
	newFileHash := args[4]
	newVersion := args[5]
	newConfigurationUUID := args[6]

	uuidIndexKey := createIndexKey(stub, uuid, "configurationfile")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "configurationfile")
	}
	configurationFileAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	configurationFile := makeConfigurationFileFromBytes(stub, configurationFileAsBytes)

	configurationFile.ReleaseDate = newReleaseDate
	configurationFile.Status = newStatus
	configurationFile.FileURL = newFileURL
	configurationFile.FileHash = newFileHash
	configurationFile.Version = newVersion

	configurationIndexKey := createIndexKey(stub, newConfigurationUUID, "configuration")
	if checkEntityExist(stub, configurationIndexKey) == false {
		return entityNotFoundMessage(stub, newConfigurationUUID, "configuration")
	}
	configurationAsBytes := getEntityFromLedger(stub, configurationIndexKey)
	configuration := makeConfigurationFromBytes(stub, configurationAsBytes)

	configurationFile.ConfigurationUuid = configuration

	configurationFileAsJSONBytes := makeBytesFromConfigurationFile(stub, configurationFile)

	putEntityInLedger(stub, uuidIndexKey, configurationFileAsJSONBytes)
	return succeed(stub, "configurationFileUpdatedEvent", configurationFileAsJSONBytes)

}

//UnregisterConfigurationFileByID method to unregister configurationfile by id
func (t *ConfigurationFile) UnregisterConfigurationFileByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nUnregisterConfigurationFileByID - Start")
	uuid := args[0]

	uuidIndexKey := createIndexKey(stub, uuid, "configurationfile")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "configurationfile")
	}
	configurationFileAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	makeConfigurationFromBytes(stub, configurationFileAsBytes)

	deleteEntityFromLedger(stub, uuidIndexKey)
	fmt.Println("Configuration File " + uuid + " was unregistered successfully")
	return succeed(stub, "configurationFileUnregisteredEvent", []byte("{\"uuid\":\""+uuid+"\"}"))
}

//GetAllConfigurationFile method to get all configurationfile
func (t *ConfigurationFile) GetAllConfigurationFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllConfigurationFile - Start:(%s)\n", args)

	objectType := args[0]
	configurationfile := ConfigurationFile{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []ConfigurationFile
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		configurationAsBytes := getEntityFromLedger(stub, responseRange.Key)
		configurationfile = makeConfigurationFileFromBytes(stub, configurationAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, configurationAsBytes)

		list = append(list, configurationfile)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of configurationsfile found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllConfigurationFileByPage method to get all configurationfile by page
func (t *ConfigurationFile) GetAllConfigurationFileByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllConfigurationFileByPage - Start:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"configurationfile"})
	panicErr(err)

	var list []ConfigurationFile

	for i := 0; i < offset && objectTypeResultsIterator.HasNext() == true; i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		configurationAsBytes := getEntityFromLedger(stub, responseRange.Key)
		configurationfile := makeConfigurationFileFromBytes(stub, configurationAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, configurationAsBytes)

		list = append(list, configurationfile)
	}
	objectTypeResultsIterator.Close()

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of configurationsfile found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllConfigurationFileByConfiguration method to get all configurationfile by configuration
func (t *ConfigurationFile) GetAllConfigurationFileByConfiguration(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllConfigurationFileByConfiguration - Start:(%s)\n", args)

	objectType := args[0]
	configurationUUID := args[1]
	configurationfile := ConfigurationFile{}
	uuidIndexKey := createIndexKey(stub, configurationUUID, "configuration")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, configurationUUID, "configuration")
	}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []ConfigurationFile
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		configurationAsBytes := getEntityFromLedger(stub, responseRange.Key)
		configurationfile = makeConfigurationFileFromBytes(stub, configurationAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, configurationAsBytes)
		if configurationfile.ConfigurationUuid.Uuid == configurationUUID {
			list = append(list, configurationfile)
		}
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of configurationsfile found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
