package main

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkCreateDevice(t *testing.T, stub *shim.MockStub,
	uuid string, name string, prettyName string, euCitizen string, cpe string, riskLevel string, providerUUID string, deviceClass string, currentConfiguration string, currentPatch string) {
	displayNewTest("Create Device")

	response := stub.MockInvoke("1", [][]byte{[]byte("CreateDevice"),
		[]byte(uuid), []byte(name), []byte(prettyName), []byte(euCitizen), []byte(cpe), []byte(riskLevel),
		[]byte(providerUUID), []byte(deviceClass), []byte(currentConfiguration), []byte(currentPatch)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetExistingDevice(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Existing Device " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetDeviceByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	d := Device{}
	_ = json.Unmarshal(response.Payload, &d)

	if d.Uuid != uuid {
		t.Fail()
	}
}

func checkGetNonExistingDevice(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get NON Existing Device " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetDeviceByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkGetCurrentPatch(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Current patch test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetCurrentPatch"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkUnregisterDevice(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Unregister device test")

	response := stub.MockInvoke("1", [][]byte{[]byte("UnregisterDeviceByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkUpdateCurrentPatch(t *testing.T, stub *shim.MockStub, patchUUID string, deviceUUID string) {
	displayNewTest("Update Current Patch test")

	response := stub.MockInvoke("1", [][]byte{[]byte("UpdateCurrentPatch"), []byte(patchUUID), []byte(deviceUUID)})

	response2 := stub.MockInvoke("1", [][]byte{[]byte("GetCurrentPatch"), []byte(deviceUUID)})

	fmt.Println(string(response2.Payload))

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllDevices(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType Device ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllDevices"),
		[]byte(objectType)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllDevicesByPage(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType Device ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllDevicesByPage"), []byte("4"), []byte("0")})

	response = stub.MockInvoke("1", [][]byte{[]byte("GetAllDevicesByPage"), []byte("2"), []byte("3")})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}
func TestDevice(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P1"), []byte("patch1"), []byte("Patch 1"),
		[]byte("today"), []byte("current"), []byte("hsttp"), []byte("1.0"), []byte("DC4")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W1"), []byte("a1"), []byte("current"), []byte("DC4")})

	checkCreateDevice(t, stub, "D1", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkGetExistingDevice(t, stub, "D1")
}

func TestCreateDevice(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P1"), []byte("patch1"), []byte("Patch 1"),
		[]byte("today"), []byte("current"), []byte("hsttp"), []byte("1.0"), []byte("DC4")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W1"), []byte("a1"), []byte("current"), []byte("DC4")})

	//checkCreateDevice(t, stub, "D1", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")

	checkCreateDevice(t, stub, "D1", "dname", "dpname", "e", "e", "e", "O6", "DC4", "", "")
}

func TestGetAllDevicesByPage(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P1"), []byte("patch1"), []byte("Patch 1"),
		[]byte("today"), []byte("current"), []byte("hsttp"), []byte("1.0"), []byte("DC4")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W1"), []byte("a1"), []byte("current"), []byte("DC4")})

	checkCreateDevice(t, stub, "D1", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D2", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D3", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D4", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D5", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D6", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D7", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D8", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D9", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D10", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D11", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D12", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")

	checkGetAllDevicesByPage(t, stub, "device")
}

func TestGetAllDevices(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P1"), []byte("patch1"), []byte("Patch 1"),
		[]byte("today"), []byte("current"), []byte("hsttp"), []byte("1.0"), []byte("DC4")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W1"), []byte("a1"), []byte("current"), []byte("DC4")})

	checkCreateDevice(t, stub, "D1", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D2", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D3", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkCreateDevice(t, stub, "D4", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")

	checkGetAllDevices(t, stub, "device")
}

func TestCheckUnregisterDevice(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P1"), []byte("patch1"), []byte("Patch 1"),
		[]byte("today"), []byte("current"), []byte("hsttp"), []byte("1.0"), []byte("DC4")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W1"), []byte("a1"), []byte("current"), []byte("DC4")})

	checkCreateDevice(t, stub, "D1", "dname", "dpname", "e", "e", "e", "O6", "DC4", "W1", "P1")
	checkUnregisterDevice(t, stub, "D1")

}
