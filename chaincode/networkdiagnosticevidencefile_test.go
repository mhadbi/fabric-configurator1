package main

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkGetExistingNetworkDiagnosticEvidenceFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Existing NetworkDiagnosticEvidenceFile " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetNetworkDiagnosticEvidenceFileByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	dc := NetworkDiagnosticEvidenceFile{}
	_ = json.Unmarshal(response.Payload, &dc)

	if dc.Uuid != uuid {
		t.Fail()
	}
}

func checkGetNonExistingNetworkDiagnosticEvidenceFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get NON Existing NetworkDiagnosticEvidenceFile " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetNetworkDiagnosticEvidenceFileByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceDoesNotExist(t *testing.T, stub *shim.MockStub, uuid string,
	name string, prettyName string, evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string,
	attackStatus string, diagnosticApplicationName string, diagnosticApplicationVersion string, diagnosticApplicationResult string, evidence string) {
	displayNewTest("Create NetworkDiagnosticEvidenceFile when Evidence Org DOES NOT Exist")
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateNetworkDiagnosticEvidenceFile"), []byte(uuid), []byte(name), []byte(prettyName), []byte(evidenceFileDataSourceTitle),
		[]byte(evidenceFileInformation), []byte(evidenceFileCreationDate), []byte(attackStatus), []byte(diagnosticApplicationName), []byte(diagnosticApplicationVersion), []byte(diagnosticApplicationResult), []byte(evidence)})
	if response.Status == shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t *testing.T, stub *shim.MockStub, uuid string,
	name string, prettyName string, evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string,
	attackStatus string, diagnosticApplicationName string, diagnosticApplicationVersion string, diagnosticApplicationResult string, evidence string) {
	displayNewTest("Create NetworkDiagnosticEvidenceFile when Evidence Exist")
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateNetworkDiagnosticEvidenceFile"), []byte(uuid), []byte(name), []byte(prettyName), []byte(evidenceFileDataSourceTitle),
		[]byte(evidenceFileInformation), []byte(evidenceFileCreationDate), []byte(attackStatus), []byte(diagnosticApplicationName), []byte(diagnosticApplicationVersion), []byte(diagnosticApplicationResult), []byte(evidence)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllNetworkDiagnosticEvidenceFile(t *testing.T, stub *shim.MockStub, objectType string, numberOfDeviceCreated int) {
	displayNewTest("GetAllByObjectType NetworkDiagnosticEvidenceFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllNetworkDiagnosticEvidenceFile"),
		[]byte(objectType)})

	var liste []NetworkDiagnosticEvidenceFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil || len(liste) != numberOfDeviceCreated {
		t.Fail()
	}
}

func checkGetAllNetworkDiagnosticEvidenceFileWithPage(t *testing.T, stub *shim.MockStub, pageSize string, offset string) {
	displayNewTest("GetAllByObjectTypeWithpage NetworkDiagnosticEvidenceFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllNetworkDiagnosticEvidenceFileByPage"), []byte(pageSize), []byte(offset)})
	var liste []NetworkDiagnosticEvidenceFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t, stub, "NDE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E0")
}

func TestCreateNetworkDiagnosticEvidenceFileWhenEvidenceDoesNotExist(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceDoesNotExist(t, stub, "NDE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E0")
}

func TestGetExistingNetworkDiagnosticEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t, stub, "NDE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E0")

	checkGetExistingNetworkDiagnosticEvidenceFile(t, stub, "NDE0")

}

func TestGetNonExistingNetworkDiagnosticEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	checkGetNonExistingNetworkDiagnosticEvidenceFile(t, stub, "NDE0")
}

func TestGetAllNetworkDiagnosticEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t, stub, "NDE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E0")
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t, stub, "NDE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E0")

	checkGetAllNetworkDiagnosticEvidenceFile(t, stub, "networkDiagnosticEvidenceFile", 2)
}

func TestGetAllNetworkDiagnosticEvidenceFileWithPage(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E1"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t, stub, "NDE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E0")
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t, stub, "NDE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E0")
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t, stub, "IE2", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E1")
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t, stub, "IE3", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E1")

	checkGetAllNetworkDiagnosticEvidenceFileWithPage(t, stub, "100", "0")
}
