package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type SoftwareEvidenceFile struct {
	ObjectType      string
	SubObjectType   string
	Uuid            string
	SoftwareName    string
	SoftwareVersion string
	SoftwareType    string
	EvidenceFile    EvidenceFile
	DeviceLogs      []string
}

func createSoftwareEvidenceFileOnLedger(stub shim.ChaincodeStubInterface, objectType string, uuid string, name string, prettyName string,
	evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string, attackStatus string,
	softwareName string, softwareVersion string, softwareType string, evidence Evidence, deviceLogs []string) []byte {

	evidenceFile := EvidenceFile{name, prettyName, evidenceFileDataSourceTitle,
		evidenceFileInformation, evidenceFileCreationDate, attackStatus, evidence}

	softwareEvidenceFile := SoftwareEvidenceFile{objectType, "softwareEvidenceFile", uuid, softwareName, softwareVersion, softwareType, evidenceFile, deviceLogs}
	softwareEvidenceFileAsJSONBytes := makeBytesFromSoftwareEvidenceFile(stub, softwareEvidenceFile)
	uuidIndexKey2 := createIndexKey2(stub, []string{"evidenceFile", "softwareEvidenceFile", uuid})
	putEntityInLedger(stub, uuidIndexKey2, softwareEvidenceFileAsJSONBytes)

	return softwareEvidenceFileAsJSONBytes
}

func makeSoftwareEvidenceFileFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) SoftwareEvidenceFile {
	softwareEvidenceFile := SoftwareEvidenceFile{}
	err := json.Unmarshal(bytes, &softwareEvidenceFile)
	panicErr(err)
	return softwareEvidenceFile
}

func makeBytesFromSoftwareEvidenceFile(stub shim.ChaincodeStubInterface, softwareEvidenceFile SoftwareEvidenceFile) []byte {
	bytes, err := json.Marshal(softwareEvidenceFile)
	panicErr(err)
	return bytes
}

//CreateSoftwareEvidenceFile method to create an softwareEvidenceFile
func (t *SoftwareEvidenceFile) CreateSoftwareEvidenceFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n CreateSoftwareEvidenceFile - Start")
	uuid := args[0]
	name := args[1]
	prettyName := args[2]
	evidenceFileDataSourceTitle := args[3]
	evidenceFileInformation := args[4]
	evidenceFileCreationDate := args[5]
	attackStatus := args[6]
	softwareName := args[7]
	softwareVersion := args[8]
	softwareType := args[9]
	evidence := args[10]
	deviceLogsString := args[11]

	uuidIndexKey := createIndexKey2(stub, []string{"evidenceFile", "softwareEvidenceFile", uuid})
	if checkEntityExist(stub, uuidIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "evidenceFile")
	}
	evidenceIndexKey := createIndexKey(stub, evidence, "evidence")
	if checkEntityExist(stub, evidenceIndexKey) == false {
		return entityNotFoundMessage(stub, evidence, "evidence")
	}

	deviceLogs := strings.Fields(deviceLogsString)

	for i := range deviceLogs {
		loguuidIndexKey := createIndexKey(stub, deviceLogs[i], "devicelog")
		if checkEntityExist(stub, loguuidIndexKey) == false {
			return entityNotFoundMessage(stub, deviceLogs[i], "devicelog")
		}
	}

	evidenceAsBytes := getEntityFromLedger(stub, evidenceIndexKey)
	evidenceUUID := makeEvidenceFromBytes(stub, evidenceAsBytes)

	createdSoftwareEvidenceFile := createSoftwareEvidenceFileOnLedger(stub, "evidenceFile", uuid, name,
		prettyName, evidenceFileDataSourceTitle, evidenceFileInformation, evidenceFileCreationDate, attackStatus, softwareName, softwareVersion, softwareType, evidenceUUID, deviceLogs)
	return succeed(stub, "softwareEvidenceFileCreatedEvent", createdSoftwareEvidenceFile)
}

//GetSoftwareEvidenceFileByID method to get an softwareEvidenceFile by id
func (t *SoftwareEvidenceFile) GetSoftwareEvidenceFileByID(stub shim.ChaincodeStubInterface, args string) pb.Response {
	fmt.Println("\n GetSoftwareEvidenceFileByID - Start", args)

	uuid := args

	uuidIndexKey := createIndexKey2(stub, []string{"evidenceFile", "softwareEvidenceFile", uuid})
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "evidenceFile")
	}
	softwareEvidenceFileAsBytes := getEntityFromLedger(stub, uuidIndexKey)

	return shim.Success(softwareEvidenceFileAsBytes)
}

//GetAllSoftwareEvidenceFile Method to get all softwareEvidenceFiles from the ledger
func (t *SoftwareEvidenceFile) GetAllSoftwareEvidenceFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllSoftwareEvidenceFile:(%s)\n", args)
	objectType := args[0]
	softwareEvidenceFile := SoftwareEvidenceFile{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~subObjectType~uuid", []string{objectType, "softwareEvidenceFile"})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []SoftwareEvidenceFile
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		softwareEvidenceFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		softwareEvidenceFile = makeSoftwareEvidenceFileFromBytes(stub, softwareEvidenceFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, softwareEvidenceFileAsBytes)

		list = append(list, softwareEvidenceFile)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of softwareEvidenceFiles found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllSoftwareEvidenceFileByPage Method to get all softwareEvidenceFiles from the ledger
func (t *SoftwareEvidenceFile) GetAllSoftwareEvidenceFileByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllSoftwareEvidenceFilesByPage:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~subObjectType~uuid", []string{"evidenceFile", "softwareEvidenceFile"})
	panicErr(err)

	var list []SoftwareEvidenceFile

	for i := 0; i < offset && objectTypeResultsIterator.HasNext() == true; i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		softwareEvidenceFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		softwareEvidenceFile := makeSoftwareEvidenceFileFromBytes(stub, softwareEvidenceFileAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, softwareEvidenceFileAsBytes)

		list = append(list, softwareEvidenceFile)
	}
	objectTypeResultsIterator.Close()

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of softwareEvidenceFiles found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
