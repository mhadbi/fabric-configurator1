package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//Evidence declaration of the srtuct
type Evidence struct {
	ObjectType            string
	Uuid                  string
	Name                  string
	PrettyName            string
	EvidenceDate          string
	EvidenceURL           string
	TargetedVulnerability string
	TypeOfAttack          string
}

func createEvidenceOnLedger(stub shim.ChaincodeStubInterface, objectType string, uuid string, name string, prettyName string,
	evidenceDate string, evidenceURL string, targetedVulnerability string, typeOfAttack string) []byte {

	evidence := Evidence{objectType, uuid, name, prettyName, evidenceDate, evidenceURL, targetedVulnerability, typeOfAttack}
	evidenceAsJSONBytes := makeBytesFromEvidence(stub, evidence)

	uuidIndexKey := createIndexKey(stub, uuid, "evidence")
	putEntityInLedger(stub, uuidIndexKey, evidenceAsJSONBytes)
	return evidenceAsJSONBytes
}

func makeEvidenceFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) Evidence {
	evidence := Evidence{}
	err := json.Unmarshal(bytes, &evidence)
	panicErr(err)
	return evidence
}

func makeBytesFromEvidence(stub shim.ChaincodeStubInterface, evidence Evidence) []byte {
	bytes, err := json.Marshal(evidence)
	panicErr(err)
	return bytes
}

//CreateEvidence method to create an evidence
func (t *Evidence) CreateEvidence(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n CreateEvidence - Start")

	uuid := args[0]
	name := args[1]
	prettyName := args[2]
	evidenceDate := args[3]
	evidenceURL := args[4]
	targetedVulnerability := args[5]
	typeOfAttack := args[6]

	uuidIndexKey := createIndexKey(stub, uuid, "evidence")
	if checkEntityExist(stub, uuidIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "evidence")
	}

	createdEvidence := createEvidenceOnLedger(stub, "evidence", uuid, name, prettyName,
		evidenceDate, evidenceURL, targetedVulnerability, typeOfAttack)
	return succeed(stub, "evidenceCreatedEvent", createdEvidence)
}

//GetEvidenceByID method to get an evidence by id
func (t *Evidence) GetEvidenceByID(stub shim.ChaincodeStubInterface, args string) pb.Response {
	fmt.Println("\n GetEvidenceByID - Start", args)

	uuid := args

	uuidIndexKey := createIndexKey(stub, uuid, "evidence")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "evidence")
	}
	evidenceAsBytes := getEntityFromLedger(stub, uuidIndexKey)

	return shim.Success(evidenceAsBytes)
}

//GetAllEvidences Method to get all evidences from the ledger
func (t *Evidence) GetAllEvidences(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllEvidences:(%s)\n", args)
	objectType := args[0]
	evidence := Evidence{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []Evidence
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		evidenceAsBytes := getEntityFromLedger(stub, responseRange.Key)
		evidence = makeEvidenceFromBytes(stub, evidenceAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, evidenceAsBytes)

		list = append(list, evidence)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of evidences found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllEvidencesByPage Method to get all evidences from the ledger
func (t *Evidence) GetAllEvidencesByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllEvidencesByPage:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"evidence"})
	panicErr(err)

	var list []Evidence

	for i := 0; i < offset && objectTypeResultsIterator.HasNext() == true; i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		evidenceAsBytes := getEntityFromLedger(stub, responseRange.Key)
		evidence := makeEvidenceFromBytes(stub, evidenceAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, evidenceAsBytes)

		list = append(list, evidence)
	}
	objectTypeResultsIterator.Close()

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of evidences found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
