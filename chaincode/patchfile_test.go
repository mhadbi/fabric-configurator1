package main

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkCreatePatchFile(t *testing.T, stub *shim.MockStub, uuid string, name string, prettyName string, fileURL string, fileHash string, version string, patchUuid string) {
	displayNewTest("Create Patch File test")

	response := stub.MockInvoke("1", [][]byte{[]byte("CreatePatchFile"), []byte(uuid), []byte(name), []byte(prettyName), []byte(fileURL), []byte(fileHash), []byte(version), []byte(patchUuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetPatchFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Patch File test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetPatchFileByID"), []byte(uuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	pf := PatchFile{}
	_ = json.Unmarshal(response.Payload, &pf)

	if pf.Uuid != uuid {
		t.Fail()
	}
}

func checkUnregisterPatchFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Unregister patch file test")

	response := stub.MockInvoke("1", [][]byte{[]byte("UnregisterPatchFileByID"), []byte(uuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllPatchFiles(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType PatchFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllPatchFiles"),
		[]byte(objectType)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllPatchFilesByPage(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType PatchFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllPatchFilesByPage"), []byte("4"), []byte("0")})

	response = stub.MockInvoke("1", [][]byte{[]byte("GetAllPatchFilesByPage"), []byte("2"), []byte("3")})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestPatchFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P1"), []byte("patch1"), []byte("Patch 1"),
		[]byte("today"), []byte("current"), []byte("http"), []byte("1.0"), []byte("DC4")})

	checkCreatePatchFile(t, stub, "F1", "file1", "File #1", "http", "123", "1.0", "P1")

	checkGetPatchFile(t, stub, "F1")
}

func TestCreatePatchFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P1"), []byte("patch1"), []byte("Patch 1"),
		[]byte("today"), []byte("current"), []byte("http"), []byte("1.0"), []byte("DC4")})

	checkCreatePatchFile(t, stub, "F1", "file1", "File #1", "http", "123", "1.0", "P1")
}

func TestGetAllPatchFilesByPage(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P1"), []byte("patch1"), []byte("Patch 1"),
		[]byte("today"), []byte("current"), []byte("hsttp"), []byte("1.0"), []byte("DC4")})

	checkCreatePatchFile(t, stub, "F1", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F2", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F3", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F4", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F5", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F6", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F7", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F8", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F9", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F10", "file1", "File #1", "http", "123", "1.0", "P1")
	checkGetAllPatchFilesByPage(t, stub, "patchfile")
}

func TestGetAllPatchFiles(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P1"), []byte("patch1"), []byte("Patch 1"),
		[]byte("today"), []byte("current"), []byte("http"), []byte("1.0"), []byte("DC4")})

	checkCreatePatchFile(t, stub, "F1", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F2", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F3", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F4", "file1", "File #1", "http", "123", "1.0", "P1")

	checkGetAllPatchFiles(t, stub, "patchfile")
}

func TestCheckUnregisterPatchFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P1"), []byte("patch1"), []byte("Patch 1"),
		[]byte("today"), []byte("current"), []byte("http"), []byte("1.0"), []byte("DC4")})

	checkCreatePatchFile(t, stub, "F1", "file1", "File #1", "http", "123", "1.0", "P1")
	checkUnregisterPatchFile(t, stub, "F1")
}
