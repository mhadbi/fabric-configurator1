package main

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkCreateConfiguration(t *testing.T, stub *shim.MockStub, uuid string, releaseDate string, status string, deviceUuid string) {
	displayNewTest("Create Configuration test ")

	response := stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte(uuid), []byte(releaseDate), []byte(status), []byte(deviceUuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetConfiguration(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Configuration test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetConfigurationByID"), []byte(uuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	c := Configuration{}
	_ = json.Unmarshal(response.Payload, &c)

	if c.Uuid != uuid {
		t.Fail()
	}
}

func checkUnregisterConfiguration(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Unregister configuration test")

	response := stub.MockInvoke("1", [][]byte{[]byte("UnregisterConfigurationByID"), []byte(uuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllConfiguration(t *testing.T, stub *shim.MockStub, objectType string, numberOfConfigurationCreated int) {
	displayNewTest("GetAllByObjectType Configuration ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllConfiguration"),
		[]byte(objectType)})

	var liste []Configuration
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil || len(liste) != numberOfConfigurationCreated {
		t.Fail()
	}
}

func checkGetAllConfigurationWithPage(t *testing.T, stub *shim.MockStub, pageSize string, offset string) {
	displayNewTest("GetAllByObjectTypeWithpage Configuration ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllConfigurationByPage"), []byte(pageSize), []byte(offset)})
	var liste []Configuration
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestCreateConfiguration(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})

	checkCreateConfiguration(t, stub, "W1", "a", "current", "DC4")

}

func TestGetConfigurationByID(t *testing.T) {
	displayNewTest("Get Configuration By Uuid ")
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})

	checkCreateConfiguration(t, stub, "W1", "a1", "current", "DC4")

	checkGetConfiguration(t, stub, "W1")
}

func TestGetAllConfiguration(t *testing.T) {
	displayNewTest("GetAllByObjectType Configuration ")
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})

	checkCreateConfiguration(t, stub, "W1", "a1", "current", "DC4")
	checkCreateConfiguration(t, stub, "W2", "a2", "current", "DC4")
	checkCreateConfiguration(t, stub, "W3", "a3", "current", "DC4")

	checkGetAllConfiguration(t, stub, "configuration", 3)
}
func TestGetAllConfigurationWithPage(t *testing.T) {
	displayNewTest("GetAllByObjectTypeWithpage Configuration ")
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})

	checkCreateConfiguration(t, stub, "W1", "a1", "current", "DC4")
	checkCreateConfiguration(t, stub, "W2", "a2", "current", "DC4")
	checkCreateConfiguration(t, stub, "W3", "a3", "current", "DC4")
	checkCreateConfiguration(t, stub, "W4", "a4", "current", "DC4")
	checkCreateConfiguration(t, stub, "W5", "a5", "current", "DC4")
	checkGetAllConfigurationWithPage(t, stub, "3", "2")
}

func TestCheckUnregisterConfiguration(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	checkCreateConfiguration(t, stub, "W1", "a1", "current", "DC4")
	checkUnregisterConfiguration(t, stub, "W1")
}

func TestCheckUnregisterConfigurationWithExistingConfigurationFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	checkCreateConfiguration(t, stub, "W1", "a1", "current", "DC4")
	checkCreateConfiguration(t, stub, "W2", "a2", "current", "DC4")

	checkCreateConfigurationFile(t, stub, "X1", "d", "current", "h", "h", "1.0", "W1")
	checkCreateConfigurationFile(t, stub, "X2", "d", "current", "h", "h", "1.0", "W1")
	checkCreateConfigurationFile(t, stub, "X3", "d", "current", "h", "h", "1.0", "W2")
	checkCreateConfigurationFile(t, stub, "X4", "d", "current", "h", "h", "1.0", "W2")

	checkUnregisterConfiguration(t, stub, "W1")
	checkGetAllConfigurationFile(t, stub, "configurationfile", 2)

}
