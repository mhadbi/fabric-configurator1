package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type EvidenceFile struct {
	Name                        string
	PrettyName                  string
	EvidenceFileDataSourceTitle string
	EvidenceFileInformation     string
	EvidenceFileCreationDate    string
	AttackStatus                string
	Evidence                    Evidence
}

//GetAllEvidenceFile Method to get all evidenceFile from the ledger
func (t *EvidenceFile) GetAllEvidenceFile(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllEvidenceFile:(%s)\n", args)
	objectType := args[0]
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~subObjectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var osList []OSEvidenceFile
	var softwareList []SoftwareEvidenceFile
	var incidentTrackingList []IncidentTrackingEvidenceFile
	var networkTrafficList []NetworkTrafficEvidenceFile
	var networkDiagnosticList []NetworkDiagnosticEvidenceFile
	type EvidenceFileList struct {
		OSFiles                []OSEvidenceFile
		SoftwareFiles          []SoftwareEvidenceFile
		NetworkDiagnosticFiles []NetworkDiagnosticEvidenceFile
		IncidentTrackingFiles  []IncidentTrackingEvidenceFile
		NetworkTrafficFiles    []NetworkTrafficEvidenceFile
	}

	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		evidenceFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		key := splitIndexKey(stub, responseRange.Key)
		subObjectType := key[1]
		switch subObjectType {
		case "osEvidenceFile":
			evidenceFile := makeOSEvidenceFileFromBytes(stub, evidenceFileAsBytes)
			osList = append(osList, evidenceFile)
		case "softwareEvidenceFile":
			evidenceFile := makeSoftwareEvidenceFileFromBytes(stub, evidenceFileAsBytes)
			softwareList = append(softwareList, evidenceFile)
		case "incidentTrackingEvidenceFile":
			evidenceFile := makeIncidentTrackingEvidenceFileFromBytes(stub, evidenceFileAsBytes)
			incidentTrackingList = append(incidentTrackingList, evidenceFile)
		case "networkDiagnosticEvidenceFile":
			evidenceFile := makeNetworkDiagnosticEvidenceFileFromBytes(stub, evidenceFileAsBytes)
			networkDiagnosticList = append(networkDiagnosticList, evidenceFile)
		case "networkTrafficEvidenceFile":
			evidenceFile := makeNetworkTrafficEvidenceFileFromBytes(stub, evidenceFileAsBytes)
			networkTrafficList = append(networkTrafficList, evidenceFile)
		}
	}
	evidenceFileList := EvidenceFileList{}
	evidenceFileList.OSFiles = osList
	evidenceFileList.SoftwareFiles = softwareList
	evidenceFileList.IncidentTrackingFiles = incidentTrackingList
	evidenceFileList.NetworkDiagnosticFiles = networkDiagnosticList

	newOrgs, err := json.Marshal(evidenceFileList)
	panicErr(err)
	fmt.Printf("\n List of evidenceFiles found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllEvidenceFileByEvidence Method to get all evidenceFile By Evidence from the ledger
func (t *EvidenceFile) GetAllEvidenceFileByEvidence(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllEvidenceFile:(%s)\n", args)
	objectType := args[0]
	evidenceUuid := args[1]
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~subObjectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var osList []OSEvidenceFile
	var softwareList []SoftwareEvidenceFile
	var incidentTrackingList []IncidentTrackingEvidenceFile
	var networkTrafficList []NetworkTrafficEvidenceFile
	var networkDiagnosticList []NetworkDiagnosticEvidenceFile
	type EvidenceFileList struct {
		OSFiles                []OSEvidenceFile
		SoftwareFiles          []SoftwareEvidenceFile
		NetworkDiagnosticFiles []NetworkDiagnosticEvidenceFile
		IncidentTrackingFiles  []IncidentTrackingEvidenceFile
		NetworkTrafficFiles    []NetworkTrafficEvidenceFile
	}

	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		evidenceFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		key := splitIndexKey(stub, responseRange.Key)
		subObjectType := key[1]
		switch subObjectType {
		case "osEvidenceFile":
			evidenceFile := makeOSEvidenceFileFromBytes(stub, evidenceFileAsBytes)
			if evidenceFile.EvidenceFile.Evidence.Uuid == evidenceUuid {
				osList = append(osList, evidenceFile)
			}
		case "softwareEvidenceFile":
			evidenceFile := makeSoftwareEvidenceFileFromBytes(stub, evidenceFileAsBytes)
			if evidenceFile.EvidenceFile.Evidence.Uuid == evidenceUuid {
				softwareList = append(softwareList, evidenceFile)
			}
		case "incidentTrackingEvidenceFile":
			evidenceFile := makeIncidentTrackingEvidenceFileFromBytes(stub, evidenceFileAsBytes)
			if evidenceFile.EvidenceFile.Evidence.Uuid == evidenceUuid {
				incidentTrackingList = append(incidentTrackingList, evidenceFile)
			}
		case "networkDiagnosticEvidenceFile":
			evidenceFile := makeNetworkDiagnosticEvidenceFileFromBytes(stub, evidenceFileAsBytes)
			if evidenceFile.EvidenceFile.Evidence.Uuid == evidenceUuid {
				networkDiagnosticList = append(networkDiagnosticList, evidenceFile)
			}
		case "networkTrafficEvidenceFile":
			evidenceFile := makeNetworkTrafficEvidenceFileFromBytes(stub, evidenceFileAsBytes)
			if evidenceFile.EvidenceFile.Evidence.Uuid == evidenceUuid {
				networkTrafficList = append(networkTrafficList, evidenceFile)
			}
		}
	}
	evidenceFileList := EvidenceFileList{}
	evidenceFileList.OSFiles = osList
	evidenceFileList.SoftwareFiles = softwareList
	evidenceFileList.IncidentTrackingFiles = incidentTrackingList
	evidenceFileList.NetworkDiagnosticFiles = networkDiagnosticList

	newOrgs, err := json.Marshal(evidenceFileList)
	panicErr(err)
	fmt.Printf("\n List of evidenceFiles found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
