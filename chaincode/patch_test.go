package main

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkCreatePatchWithValidStatus(t *testing.T, stub *shim.MockStub, uuid string, name string, prettyName string,
	releaseDate string, status string, releaseURL string, version string, deviceClass string) {
	displayNewTest("Create Patch test w/ valid status")

	response := stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte(uuid), []byte(name), []byte(prettyName),
		[]byte(releaseDate), []byte(status), []byte(releaseURL), []byte(version), []byte(deviceClass)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkCreatePatchWithInvalidStatus(t *testing.T, stub *shim.MockStub, uuid string, name string, prettyName string,
	releaseDate string, status string, releaseURL string, version string, organizationUuid string) {
	displayNewTest("Create Patch test w/ invalid status")

	response := stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte(uuid), []byte(name), []byte(prettyName),
		[]byte(releaseDate), []byte(status), []byte(releaseURL), []byte(version), []byte(organizationUuid)})

	if response.Status == shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkGetPatch(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Patch test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetPatchByID"), []byte(uuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	p := Patch{}
	_ = json.Unmarshal(response.Payload, &p)

	if p.Uuid != uuid {
		t.Fail()
	}
}

func checkUnregisterPatch(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Unregister patch test")

	response := stub.MockInvoke("1", [][]byte{[]byte("UnregisterPatchByID"), []byte(uuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllPatchs(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType Patch ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllPatchs"),
		[]byte(objectType)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllPatchsByPage(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType Patch ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllPatchsByPage"), []byte("4"), []byte("0")})

	response = stub.MockInvoke("1", [][]byte{[]byte("GetAllPatchsByPage"), []byte("2"), []byte("3")})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestPatch(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	checkCreatePatchWithValidStatus(t, stub, "P1", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")

	checkGetPatch(t, stub, "P1")

}

func TestCreatePatch(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	checkCreatePatchWithValidStatus(t, stub, "P1", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
}

func TestGetAllPatchByPage(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})

	checkCreatePatchWithValidStatus(t, stub, "P1", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P2", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P3", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P4", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P5", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P6", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P7", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P8", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P9", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P10", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkGetAllPatchsByPage(t, stub, "patch")
}

func TestGetAllPatch(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})

	checkCreatePatchWithValidStatus(t, stub, "P1", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P2", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P3", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P4", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")

	checkGetAllPatchs(t, stub, "patch")
}

func TestCheckUnregisterPatch(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	checkCreatePatchWithValidStatus(t, stub, "P1", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkUnregisterPatch(t, stub, "P1")
}

func TestCheckUnregisterPatchWithExistingPatchFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	checkCreatePatchWithValidStatus(t, stub, "P1", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchWithValidStatus(t, stub, "P2", "patch1", "Patch 1", "today", "current", "http", "1.0", "DC4")
	checkCreatePatchFile(t, stub, "F1", "file1", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F2", "file2", "File #1", "http", "123", "1.0", "P1")
	checkCreatePatchFile(t, stub, "F3", "file3", "File #1", "http", "123", "1.0", "P2")
	checkCreatePatchFile(t, stub, "F4", "file4", "File #1", "http", "123", "1.0", "P2")

	checkUnregisterPatch(t, stub, "P1")
	checkGetAllPatchFiles(t, stub, "patchfile")

}
