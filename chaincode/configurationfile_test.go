package main

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkCreateConfigurationFile(t *testing.T, stub *shim.MockStub, uuid string, releaseDate string, status string, fileUrl string, fileHash string, version string, configurationUuid string) {
	displayNewTest("Create Config File test")

	response := stub.MockInvoke("1", [][]byte{[]byte("CreateConfigurationFile"), []byte(uuid), []byte(releaseDate), []byte(status), []byte(fileUrl), []byte(fileHash), []byte(version), []byte(configurationUuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetConfigurationFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Config File test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetConfigurationFileByID"), []byte(uuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	cf := ConfigurationFile{}
	_ = json.Unmarshal(response.Payload, &cf)

	if cf.Uuid != uuid {
		t.Fail()
	}
}

func checkUnregisterConfigurationFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Unregister config file test")

	response := stub.MockInvoke("1", [][]byte{[]byte("UnregisterConfigurationFileByID"), []byte(uuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}
func checkGetAllConfigurationFile(t *testing.T, stub *shim.MockStub, objectType string, numberOfConfigurationFileCreated int) {
	displayNewTest("GetAllByObjectType Configuration file ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllConfigurationFile"),
		[]byte(objectType)})

	var liste []ConfigurationFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil || len(liste) != numberOfConfigurationFileCreated {
		t.Fail()
	}
}

func checkGetAllConfigurationFileWithPage(t *testing.T, stub *shim.MockStub, pageSize string, offset string) {
	displayNewTest("GetAllByObjectTypeWithpage ConfigurationFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllConfigurationFileByPage"), []byte(pageSize), []byte(offset)})
	var liste []ConfigurationFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestCreateConfigurationFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W1"), []byte("a1"), []byte("current"), []byte("DC4")})

	checkCreateConfigurationFile(t, stub, "X1", "d", "current", "h", "h", "1.0", "W1")

}

func TestGetConfigurationFileByID(t *testing.T) {
	displayNewTest("Get ConfigurationFile By Uuid ")
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W1"), []byte("a1"), []byte("current"), []byte("DC4")})

	checkCreateConfigurationFile(t, stub, "X1", "d", "current", "h", "h", "1.0", "W1")
	checkGetConfigurationFile(t, stub, "X1")
}

func TestGetAllConfigurationFile(t *testing.T) {
	displayNewTest("GetAllByObjectType ConfigurationFile ")
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W1"), []byte("a1"), []byte("current"), []byte("DC4")})

	checkCreateConfigurationFile(t, stub, "X1", "d", "current", "h", "h", "1.0", "W1")
	checkCreateConfigurationFile(t, stub, "X2", "d", "current", "h", "h", "1.0", "W1")

	checkGetAllConfigurationFile(t, stub, "configurationfile", 2)
}
func TestGetAllConfigurationFileWithPage(t *testing.T) {
	displayNewTest("GetAllByObjectTypeWithpage ConfigurationFile ")
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W1"), []byte("a1"), []byte("current"), []byte("DC4")})

	checkCreateConfigurationFile(t, stub, "X1", "d", "current", "h", "h", "1.0", "W1")
	checkCreateConfigurationFile(t, stub, "X2", "d", "current", "h", "h", "1.0", "W1")
	checkCreateConfigurationFile(t, stub, "X3", "d", "current", "h", "h", "1.0", "W1")
	checkCreateConfigurationFile(t, stub, "X4", "d", "current", "h", "h", "1.0", "W1")
	checkCreateConfigurationFile(t, stub, "X5", "d", "current", "h", "h", "1.0", "W1")
	checkGetAllConfigurationFileWithPage(t, stub, "3", "2")
}

func TestCheckUnregisterConfigurationFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W1"), []byte("a1"), []byte("current"), []byte("DC4")})

	checkCreateConfigurationFile(t, stub, "X1", "d", "current", "h", "h", "1.0", "W1")
	checkUnregisterConfigurationFile(t, stub, "X1")

}
