package main

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkCreateNewOrganization(t *testing.T, stub *shim.MockStub, uuid string,
	name string, prettyName string, typeOfOrganization string) {
	displayNewTest("Create Organization Test When Organization does not exist")

	response := stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"),
		[]byte(uuid), []byte(name), []byte(prettyName), []byte(typeOfOrganization)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetExistingOrganization(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Existing Organization " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetOrganizationByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	org := Organization{}
	_ = json.Unmarshal(response.Payload, &org)

	if org.Uuid != uuid {
		t.Fail()
	}
}

func checkUnregisterOrganization(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Unregister existing organization " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("UnregisterOrganizationByID"), []byte(uuid)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkUpdateOrganization(t *testing.T, stub *shim.MockStub, uuid string, name string, prettyName string, typeOfOrganization string) {
	displayNewTest("checkUpdateOrganization")
	res := stub.MockInvoke("1", [][]byte{[]byte("UpdateOrganizationByID"), []byte(uuid), []byte(name), []byte(prettyName), []byte(typeOfOrganization)})
	if res.Status != shim.OK {
		fmt.Println("UpdadeOrganization", uuid, "failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("UpdadeOrganization", uuid, "failed to get value")
		t.FailNow()
	}
}

func checkGetAllOrganizations(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType Organization ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllOrganizations"),
		[]byte(objectType)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllOrganizationsByPage(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType Organization ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllOrganizationsByPage"), []byte("4"), []byte("0")})

	response = stub.MockInvoke("1", [][]byte{[]byte("GetAllOrganizationsByPage"), []byte("2"), []byte("3")})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestCreateOrganization(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	checkCreateNewOrganization(t, stub, "O0", "SCHAIN", "Scorechain", "ISP")
	/* checkCreateNewOrganization(t, stub, "O0", "SCHAIN", "Scorechain", "ISP") */
}

func TestGetOrganizationByKey(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	checkCreateNewOrganization(t, stub, "O0", "SCHAIN", "Scorechain", "ISP")
	checkGetExistingOrganization(t, stub, "O0")
}
func TestUpdateOrganizationByKey(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	checkCreateNewOrganization(t, stub, "O0", "SCHAIN", "Scorechain", "ISP")

	checkUpdateOrganization(t, stub, "O0", "Scorechain Luxembourg", "Scorechain", "ISP")
	checkGetExistingOrganization(t, stub, "O0")
}

func TestGetAllOrganizations(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	checkCreateNewOrganization(t, stub, "O0", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O1", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O2", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O3", "SCHAIN", "Scorechain", "ISP")

	checkGetAllOrganizations(t, stub, "organization")
}

func TestGetAllOrganizationsByPage(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	checkCreateNewOrganization(t, stub, "O0", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O1", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O2", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O3", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O4", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O5", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O6", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O7", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O8", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O9", "SCHAIN", "Scorechain", "ISP")

	checkGetAllOrganizationsByPage(t, stub, "organization")
}

func TestUnregisterOrganizationWithAdministrator(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	checkCreateNewOrganization(t, stub, "O0", "SCHAIN", "Scorechain", "ISP")
	checkCreateNewOrganization(t, stub, "O1", "SCHAIN", "Scorechain", "ISP")

	checkCreateAdministratorWhenOrganizationExists(t, stub, "A1", "O0", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A2", "O1", "1")
	checkCreateAdministratorWhenOrganizationExists(t, stub, "A3", "O0", "1")

	checkUnregisterOrganization(t, stub, "O0")

	checkGetAllAdministrators(t, stub, "administrator")
}

func TestUnregisterOrganization(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	checkCreateNewOrganization(t, stub, "O0", "SCHAIN", "Scorechain", "ISP")

	checkUnregisterOrganization(t, stub, "O0")
}
