package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//Configuration declaration of the struct
type Configuration struct {
	ObjectType  string
	Uuid        string
	ReleaseDate string
	Status      string
	DeviceClass DeviceClass
}

func createConfigurationOnLedger(
	stub shim.ChaincodeStubInterface,
	uuid string,
	releaseDate string,
	status string,
	deviceClass DeviceClass,
) []byte {
	if status != "current" && status != "obsolete" && status != "vulnerable" {
		panicErr(errors.New("Status cannot have this value: it must be one of 'current', 'obsolete' or 'vulnerable'"))
	}

	configuration := Configuration{"configuration", uuid, releaseDate, status, deviceClass}
	configAsJSONBytes := makeBytesFromConfiguration(stub, configuration)

	uuidIndexKey := createIndexKey(stub, configuration.Uuid, configuration.ObjectType)

	putEntityInLedger(stub, uuidIndexKey, configAsJSONBytes)
	return configAsJSONBytes
}

func makeConfigurationFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) Configuration {
	configuration := Configuration{}
	err := json.Unmarshal(bytes, &configuration)
	panicErr(err)
	return configuration
}

func makeBytesFromConfiguration(stub shim.ChaincodeStubInterface, configuration Configuration) []byte {
	bytes, err := json.Marshal(configuration)
	panicErr(err)
	return bytes
}

//CreateConfiguration method to create a configuration
func (t *Configuration) CreateConfiguration(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n CreateConfiguration - Start")

	uuid := args[0]
	releaseDate := args[1]
	status := args[2]
	deviceClass := args[3]

	uuidIndexKey := createIndexKey(stub, uuid, "configuration")
	if checkEntityExist(stub, uuidIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "configuration")
	}

	deviceClassIndexKey := createIndexKey(stub, deviceClass, "deviceclass")
	if checkEntityExist(stub, deviceClassIndexKey) == false {
		return entityNotFoundMessage(stub, deviceClass, "deviceclass")
	}
	deviceClassAsBytes := getEntityFromLedger(stub, deviceClassIndexKey)
	deviceClassObj := makeDeviceClassFromBytes(stub, deviceClassAsBytes)

	createdConfigurationAsBytes := createConfigurationOnLedger(stub, uuid, releaseDate, status, deviceClassObj)
	return succeed(stub, "configurationCreatedEvent", createdConfigurationAsBytes)
}

//GetConfigurationByID method to get configuration from ledger by id
func (t *Configuration) GetConfigurationByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n GetConfigurationByID - Start")

	uuid := args[0]
	uuidIndexKey := createIndexKey(stub, uuid, "configuration")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "configuration")
	}
	configurationAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	return shim.Success(configurationAsBytes)
}

//UpdateConfigurationByID method to update an configuration by id
func (t *Configuration) UpdateConfigurationByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n UpdateConfigurationByID - Start")

	uuid := args[0]
	newReleaseDate := args[1]
	newStatus := args[2]
	newDeviceClassUUID := args[3]

	uuidIndexKey := createIndexKey(stub, uuid, "configuration")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "configuration")
	}
	configurationAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	configuration := makeConfigurationFromBytes(stub, configurationAsBytes)

	configuration.ReleaseDate = newReleaseDate
	configuration.Status = newStatus

	deviceClassIndexKey := createIndexKey(stub, newDeviceClassUUID, "deviceclass")
	if checkEntityExist(stub, deviceClassIndexKey) == false {
		return entityNotFoundMessage(stub, newDeviceClassUUID, "deviceclass")
	}
	deviceClassAsBytes := checkEntityExistsAndGet(stub, string(deviceClassIndexKey))
	deviceClass := makeDeviceClassFromBytes(stub, deviceClassAsBytes)

	configuration.DeviceClass = deviceClass

	configurationAsJSONBytes := makeBytesFromConfiguration(stub, configuration)

	putEntityInLedger(stub, uuidIndexKey, configurationAsJSONBytes)
	return succeed(stub, "configurationUpdatedEvent", configurationAsJSONBytes)

}

//UnregisterConfigurationFileByConfiguration method to delete configurationfile by configuration
func UnregisterConfigurationFileByConfiguration(stub shim.ChaincodeStubInterface, configurationUUID string) pb.Response {
	fmt.Println("\n UnregisterConfigurationFileByConfiguration - Start")
	configurationFile := ConfigurationFile{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"configurationfile"})
	if err != nil {
		return shim.Error(err.Error())
	}
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		fmt.Println("\n UnregisterConfigurationFile - Start")
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		configurationFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		configurationFile = makeConfigurationFileFromBytes(stub, configurationFileAsBytes)

		if configurationFile.ConfigurationUuid.Uuid == configurationUUID {
			configurationFileUUIDIndexKey := createIndexKey(stub, configurationFile.Uuid, "configurationfile")
			deleteEntityFromLedger(stub, configurationFileUUIDIndexKey)
			fmt.Println("ConfigurationFile " + configurationFile.Uuid + " was unregistered successfully")
		}
	}
	return shim.Success([]byte(""))
}

//UnregisterConfigurationByID method to unregister configuration from ledger by id
func (t *Configuration) UnregisterConfigurationByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n UnregisterConfigurationByID - Start")
	uuid := args[0]
	uuidIndexKey := createIndexKey(stub, uuid, "configuration")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "configuration")
	}

	//delete all configuration file by this configuration
	UnregisterConfigurationFileByConfiguration(stub, uuid)

	//delete configuration
	deleteEntityFromLedger(stub, uuidIndexKey)
	fmt.Println("Configuration " + uuid + " and all owned configuration files was successfully unregistered")
	return succeed(stub, "configurationUnregisteredEvent", []byte("{\"ConfigurationUUID\":\""+uuid+"\"}"))
}

//GetAllConfiguration method to get all configuration
func (t *Configuration) GetAllConfiguration(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllConfiguration - Start:(%s)\n", args)

	objectType := args[0]
	configuration := Configuration{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []Configuration
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		configurationAsBytes := getEntityFromLedger(stub, responseRange.Key)
		configuration = makeConfigurationFromBytes(stub, configurationAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, configurationAsBytes)

		list = append(list, configuration)
	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of configurations found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllConfigurationByPage method to get All configuration by page
func (t *Configuration) GetAllConfigurationByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllConfigurationByPage - Start:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"configuration"})
	panicErr(err)

	var list []Configuration

	for i := 0; i < offset && objectTypeResultsIterator.HasNext(); i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		configurationAsBytes := getEntityFromLedger(stub, responseRange.Key)
		configuration := makeConfigurationFromBytes(stub, configurationAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, configurationAsBytes)

		list = append(list, configuration)
	}
	objectTypeResultsIterator.Close()

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of configurations found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}

//GetAllConfigurationByDeviceClass method to get All configuration by deviceclass
func (t *Configuration) GetAllConfigurationByDeviceClass(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\n GetAllAdministratorsByOrganization - Start:(%s)\n", args)

	objectType := args[0]
	deviceClass := args[1]
	configuration := Configuration{}

	uuidIndexKey := createIndexKey(stub, deviceClass, "deviceclass")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, deviceClass, "devicecLass")
	}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{objectType})

	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []Configuration
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		configurationAsBytes := getEntityFromLedger(stub, responseRange.Key)
		configuration = makeConfigurationFromBytes(stub, configurationAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, configurationAsBytes)
		if configuration.DeviceClass.Uuid == deviceClass {
			list = append(list, configuration)
		}

	}

	newOrgs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of administrators By Organization found:\n%s\n", newOrgs)
	return shim.Success([]byte(newOrgs))
}
