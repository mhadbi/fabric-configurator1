package main

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkGetAllEvidenceFile(t *testing.T, stub *shim.MockStub, objectType string, numberOfDeviceCreated int) {
	displayNewTest("GetAllByObjectType EvidenceFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllEvidenceFile"),
		[]byte(objectType)})

	var liste []OSEvidenceFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}
func checkGetAllEvidenceFileByEvidence(t *testing.T, stub *shim.MockStub, objectType string, evidenceuuid string, numberOfDeviceCreated int) {
	displayNewTest("GetAllByObjectType EvidenceFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllEvidenceFileByEvidence"),
		[]byte(objectType), []byte(evidenceuuid)})

	var liste []OSEvidenceFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestGetAllEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	mockTest(t, stub)
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte("DL1"), []byte("name"), []byte("typeOfDeviceLog"), []byte("format"), []byte("dateStart"),
		[]byte("dateEnd"), []byte("dateOfficial"), []byte("storageURL"), []byte("fileHash"), []byte("D1")})

	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	//Osregister
	checkCreateOSEvidenceFileWhenEvidenceExist(t, stub, "OSE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E0", []string{"DL1"})
	checkCreateOSEvidenceFileWhenEvidenceExist(t, stub, "OSE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E0", []string{"DL1"})
	//software register
	checkCreateSoftwareEvidenceFileWhenEvidenceExist(t, stub, "SOE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E0", []string{"DL1"})
	checkCreateSoftwareEvidenceFileWhenEvidenceExist(t, stub, "SOE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E0", []string{"DL1"})
	//incident register
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t, stub, "IE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E0")
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t, stub, "IE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E0")
	//networkDiagnostic
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t, stub, "NDE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E0")
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t, stub, "NDE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E0")
	//networktracking
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NTE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E0")
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NTE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E0")

	checkGetAllEvidenceFile(t, stub, "evidenceFile", 10)
}

func TestGetAllEvidenceFileByEvidence(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	mockTest(t, stub)
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte("DL1"), []byte("name"), []byte("typeOfDeviceLog"), []byte("format"), []byte("dateStart"),
		[]byte("dateEnd"), []byte("dateOfficial"), []byte("storageURL"), []byte("fileHash"), []byte("D1")})

	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E1"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	//Osregister
	checkCreateOSEvidenceFileWhenEvidenceExist(t, stub, "OSE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E1", []string{"DL1"})
	checkCreateOSEvidenceFileWhenEvidenceExist(t, stub, "OSE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E0", []string{"DL1"})
	//software register
	checkCreateSoftwareEvidenceFileWhenEvidenceExist(t, stub, "SOE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E1", []string{"DL1"})
	checkCreateSoftwareEvidenceFileWhenEvidenceExist(t, stub, "SOE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "TYPE", "E0", []string{"DL1"})
	//incident register
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t, stub, "IE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E1")
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t, stub, "IE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E0")
	//networkDiagnostic
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t, stub, "NDE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E1")
	checkCreateNetworkDiagnosticEvidenceFileWhenEvidenceExist(t, stub, "NDE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "RESULT", "E0")
	//networktracking
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NTE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E1")
	checkCreateNetworkTrafficEvidenceFileWhenEvidenceExist(t, stub, "NTE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status",
		"SourceIP", "DestinationIP", "Protocol", "Port", "StatusCode", "E0")

	checkGetAllEvidenceFileByEvidence(t, stub, "evidenceFile", "E1", 3)
}
