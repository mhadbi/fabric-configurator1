package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//PatchStatus declaration
type PatchStatus string

//Differents values of PatchStatus declaration
const (
	CURRENT    PatchStatus = "current"
	OBSOLETE   PatchStatus = "obsolete"
	VULNERABLE PatchStatus = "vulnerable"
)

//Patch struct declaration
type Patch struct {
	ObjectType  string
	Uuid        string
	Name        string
	PrettyName  string
	ReleaseDate string
	Status      PatchStatus
	ReleaseURL  string
	Version     string
	DeviceClass DeviceClass
}

func createPatchOnLedger(
	stub shim.ChaincodeStubInterface,
	objectType string,
	uuid string,
	name string,
	prettyName string,
	releaseDate string,
	status PatchStatus,
	releaseURL string,
	version string,
	deviceClass DeviceClass,
) []byte {
	if status != CURRENT && status != OBSOLETE && status != VULNERABLE {
		panicErr(errors.New("Status cannot have this value: it must be one of 'current', 'obsolete' or 'vulnerable'"))
	}
	patch := Patch{objectType, uuid, name, prettyName, releaseDate, status, releaseURL, version, deviceClass}
	patchAsJSONBytes := makeBytesFromPatch(stub, patch)

	uuidIndexKey := createIndexKey(stub, patch.Uuid, patch.ObjectType)
	putEntityInLedger(stub, uuidIndexKey, patchAsJSONBytes)
	return patchAsJSONBytes
}

func makePatchFromBytes(stub shim.ChaincodeStubInterface, bytes []byte) Patch {
	patch := Patch{}
	err := json.Unmarshal(bytes, &patch)
	panicErr(err)
	return patch
}

func makeBytesFromPatch(stub shim.ChaincodeStubInterface, patch Patch) []byte {
	bytes, err := json.Marshal(patch)
	panicErr(err)
	return bytes
}

//CreatePatch method to create patch
func (t *Patch) CreatePatch(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nCreatePatch - Start")

	uuid := args[0]
	name := args[1]
	prettyName := args[2]
	releaseDate := args[3]
	status := PatchStatus(args[4])
	releaseURL := args[5]
	version := args[6]
	deviceClass := args[7]

	patchUUIDIndexKey := createIndexKey(stub, uuid, "patch")
	if checkEntityExist(stub, patchUUIDIndexKey) == true {
		return entityAlreadyExistMessage(stub, uuid, "patch")
	}

	deviceClassIndexKey := createIndexKey(stub, deviceClass, "deviceclass")
	if checkEntityExist(stub, deviceClassIndexKey) == false {
		return entityNotFoundMessage(stub, deviceClass, "deviceclass")
	}
	deviceClassAsBytes := getEntityFromLedger(stub, deviceClassIndexKey)
	deviceClassObj := makeDeviceClassFromBytes(stub, deviceClassAsBytes)

	createdPatchAsBytes := createPatchOnLedger(stub, "patch", uuid, name, prettyName, releaseDate,
		status, releaseURL, version, deviceClassObj)
	return succeed(stub, "patchCreatedEvent", createdPatchAsBytes)
}

//GetPatchByID method to get Patch from ledger
func (t *Patch) GetPatchByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nGetPatchByID - Start")

	uuid := args[0]
	uuidIndexKey := createIndexKey(stub, uuid, "patch")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "patch")
	}
	patchAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	return shim.Success(patchAsBytes)
}

//UpdatePatchByID method to update patch
func (t *Patch) UpdatePatchByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n UpdatePatchFileByID - Start")

	uuid := args[0]
	newName := args[1]
	newPrettyName := args[2]
	newReleaseDate := args[3]
	newStatus := PatchStatus(args[4])
	newReleaseURL := args[5]
	newVersion := args[6]
	newDeviceClassUUID := args[7]

	uuidIndexKey := createIndexKey(stub, uuid, "patch")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "patch")
	}
	patchFileAsBytes := getEntityFromLedger(stub, uuidIndexKey)
	patch := makePatchFromBytes(stub, patchFileAsBytes)

	patch.Name = newName
	patch.PrettyName = newPrettyName
	patch.ReleaseDate = newReleaseDate
	patch.Status = newStatus
	patch.ReleaseURL = newReleaseURL
	patch.Version = newVersion
	patch.ReleaseDate = newReleaseDate

	deviceClassIndexKey := createIndexKey(stub, newDeviceClassUUID, "deviceclass")
	if checkEntityExist(stub, deviceClassIndexKey) == false {
		return entityNotFoundMessage(stub, newDeviceClassUUID, "deviceclass")
	}
	deviceClassAsBytes := getEntityFromLedger(stub, deviceClassIndexKey)
	deviceClass := makeDeviceClassFromBytes(stub, deviceClassAsBytes)

	patch.DeviceClass = deviceClass
	patchAsJSONBytes := makeBytesFromPatch(stub, patch)

	putEntityInLedger(stub, uuidIndexKey, patchAsJSONBytes)
	return succeed(stub, "patchUpdatedEvent", patchAsJSONBytes)

}

//UnregisterPatchFileByPatch method to unregister pactchfiles by patch
func UnregisterPatchFileByPatch(stub shim.ChaincodeStubInterface, patchUUID string) pb.Response {
	fmt.Println("\n UnregisterPatchFileByPatch - Start")
	patchFile := PatchFile{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"patchfile"})
	if err != nil {
		return shim.Error(err.Error())
	}
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		fmt.Println("\n UnregisterPatchFile - Start")
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		patchFileAsBytes := getEntityFromLedger(stub, responseRange.Key)
		patchFile = makePatchFileFromBytes(stub, patchFileAsBytes)

		if patchFile.Patch.Uuid == patchUUID {
			patchFileUUIDIndexKey := createIndexKey(stub, patchFile.Uuid, "patchfile")
			deleteEntityFromLedger(stub, patchFileUUIDIndexKey)
			fmt.Println("PatchFile " + patchFile.Uuid + " was unregistered successfully")
		}
	}
	return shim.Success([]byte(""))
}

//UnregisterPatchByID method to unregister the patch by id
func UnregisterPatchByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\n UnregisterPatchByID - Start")
	uuid := args[0]
	uuidIndexKey := createIndexKey(stub, uuid, "patch")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, uuid, "patch")
	}
	//delete all patch file by this patch
	UnregisterPatchFileByPatch(stub, uuid)

	//delete patch
	fmt.Println("\n UnregisterPatchByID - Begin ")
	deleteEntityFromLedger(stub, uuidIndexKey)
	fmt.Println("Patch " + uuid + " along with all patch files were successfully unregistered")
	return succeed(stub, "patchUnregisteredEvent", []byte("{\"PatchUUID\":\""+uuid+"\"}"))
}

//GetAllPatchs Method to get all patchs from the ledger
func (t *Patch) GetAllPatchs(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllPatchs:(%s)\n", args)
	objectType := args[0]
	patch := Patch{}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"patch"})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []Patch
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		patchAsBytes := getEntityFromLedger(stub, responseRange.Key)
		patch = makePatchFromBytes(stub, patchAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, patchAsBytes)

		list = append(list, patch)
	}

	newPatchs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of patchs found:\n%s\n", newPatchs)
	return shim.Success([]byte(newPatchs))
}

//GetAllPatchsByPage Method to get all patchs from the ledger
func (t *Patch) GetAllPatchsByPage(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllPatchsByPage:(%s)\n", args)

	pageSize, err := strconv.Atoi(args[0])
	panicErr(err)
	offset, err := strconv.Atoi(args[1])
	panicErr(err)

	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"patch"})
	panicErr(err)

	var list []Patch

	for i := 0; i < offset && objectTypeResultsIterator.HasNext(); i++ {
		_, err := objectTypeResultsIterator.Next()
		panicErr(err)
	}

	for i := 0; i < pageSize && objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()

		panicErr(err)
		patchAsBytes := getEntityFromLedger(stub, responseRange.Key)
		patch := makePatchFromBytes(stub, patchAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, patchAsBytes)

		list = append(list, patch)
	}
	objectTypeResultsIterator.Close()

	newPatch, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of patchs found:\n%s\n", newPatch)
	return shim.Success([]byte(newPatch))
}

//GetAllPatchsByDeviceClass Method to get all patchs by an Organization from the ledger
func (t *Patch) GetAllPatchsByDeviceClass(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Printf("\nGetAllPatchs:(%s)\n", args)
	objectType := args[0]
	deviceClass := args[1]
	patch := Patch{}
	uuidIndexKey := createIndexKey(stub, deviceClass, "deviceclass")
	if checkEntityExist(stub, uuidIndexKey) == false {
		return entityNotFoundMessage(stub, deviceClass, "devicecLass")
	}
	objectTypeResultsIterator, err := stub.GetStateByPartialCompositeKey("objectType~uuid", []string{"patch"})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer objectTypeResultsIterator.Close()
	var list []Patch
	fmt.Printf("-Found list of %s\n ", objectType)
	var i int
	for i = 0; objectTypeResultsIterator.HasNext() == true; i++ {
		responseRange, err := objectTypeResultsIterator.Next()
		panicErr(err)
		fmt.Printf("- found from objectType:%s\n", objectType)
		patchAsBytes := getEntityFromLedger(stub, responseRange.Key)
		patch = makePatchFromBytes(stub, patchAsBytes)
		fmt.Printf("- found from uuid:%s response:%s\n", responseRange.Key, patchAsBytes)
		if patch.DeviceClass.Uuid == deviceClass {
			list = append(list, patch)
		}
	}

	newPatchs, err := json.Marshal(list)
	panicErr(err)
	fmt.Printf("\n List of patchs found:\n%s\n", newPatchs)
	return shim.Success([]byte(newPatchs))
}
