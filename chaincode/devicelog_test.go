package main

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkCreateDeviceLogWhenDeviceExists(t *testing.T, stub *shim.MockStub, uuid string, name string, typeOfDeviceLog string,
	format string, dateStart string, dateEnd string, dateOfficial string, storageURL string,
	fileHash string, creatorDevice string) {
	displayNewTest("Create DeviceLog when Parent Device Exists")

	// Request
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte(uuid), []byte(name), []byte(typeOfDeviceLog), []byte(format), []byte(dateStart),
		[]byte(dateEnd), []byte(dateOfficial), []byte(storageURL), []byte(fileHash), []byte(creatorDevice)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}
func checkCreateDeviceLogWhenDeviceDoesNotExist(t *testing.T, stub *shim.MockStub, uuid string, name string, typeOfDeviceLog string,
	format string, dateStart string, dateEnd string, dateOfficial string, storageURL string,
	fileHash string, creatorDevice string) {
	displayNewTest("Create DeviceLog when Parent Device DOES NOT Exist")

	// Request
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte(uuid), []byte(name), []byte(typeOfDeviceLog), []byte(format), []byte(dateStart),
		[]byte(dateEnd), []byte(dateOfficial), []byte(storageURL), []byte(fileHash), []byte(creatorDevice)})

	if response.Status == shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkGetExistingDeviceLog(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Existing DeviceLog " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetDeviceLogByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	deviceLog := DeviceLog{}
	_ = json.Unmarshal(response.Payload, &deviceLog)

	if deviceLog.Uuid != uuid {
		t.Fail()
	}
}

func checkGetNonExistingDeviceLog(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get NON Existing DeviceLog " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetDeviceLogByEmployeeID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkGetAllDeviceLog(t *testing.T, stub *shim.MockStub, objectType string) {
	displayNewTest("GetAllByObjectType DeviceLog ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllDeviceLog"),
		[]byte(objectType)})

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}
func mockTest(t *testing.T, stub *shim.MockStub) {
	stub.MockInvoke("1", [][]byte{[]byte("CreateOrganization"), []byte("O6"), []byte("SCHAIN"), []byte("Scorechain"), []byte("ISP")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceClass"), []byte("DC4"), []byte("dc1name"), []byte("dc1pname"), []byte("O6")})
	stub.MockInvoke("1", [][]byte{[]byte("CreatePatch"), []byte("P1"), []byte("patch1"), []byte("Patch 1"),
		[]byte("today"), []byte("current"), []byte("hsttp"), []byte("1.0"), []byte("DC4")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateConfiguration"), []byte("W1"), []byte("a1"), []byte("current"), []byte("DC4")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateDevice"), []byte("D1"), []byte("dname"), []byte("dpname"), []byte("e"), []byte("e"), []byte("e"),
		[]byte("O6"), []byte("DC4"), []byte("W1"), []byte("P1")})
}

func TestCreateDeviceLog(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	mockTest(t, stub)

	checkCreateDeviceLogWhenDeviceExists(t, stub, "DL1", "devicelog", "Type", "format", "02/12/2019", "04/12/2019",
		"05/12/2019", "http://cyber-trust.fr", "@#3DJJ3", "D1")
	checkCreateDeviceLogWhenDeviceDoesNotExist(t, stub, "DL2", "devicelog", "Type", "format", "02/12/2019",
		"04/12/2019", "05/12/2019", "http://cyber-trust.fr", "@#3DJJ3", "D")
}

func TestGetDeviceLogByID(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	mockTest(t, stub)
	checkCreateDeviceLogWhenDeviceExists(t, stub, "DL1", "devicelog", "Type", "format", "02/12/2019", "04/12/2019",
		"05/12/2019", "http://cyber-trust.fr", "@#3DJJ3", "D1")

	checkGetExistingDeviceLog(t, stub, "DL1")
	/* checkGetNonExistingDeviceLog(t, stub, "A9") */
}

func TestGetAllDeviceLogs(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	mockTest(t, stub)

	checkCreateDeviceLogWhenDeviceExists(t, stub, "DL1", "devicelog", "Type", "format", "02/12/2019", "04/12/2019", "05/12/2019", "http://cyber-trust.fr", "@#3DJJ3", "D1")
	checkCreateDeviceLogWhenDeviceExists(t, stub, "DL2", "devicelog", "Type", "format", "02/12/2019", "04/12/2019", "05/12/2019", "http://cyber-trust.fr", "@#3DJJ3", "D1")
	checkCreateDeviceLogWhenDeviceExists(t, stub, "DL3", "devicelog", "Type", "format", "02/12/2019", "04/12/2019", "05/12/2019", "http://cyber-trust.fr", "@#3DJJ3", "D1")
	checkCreateDeviceLogWhenDeviceExists(t, stub, "DL4", "devicelog", "Type", "format", "02/12/2019", "04/12/2019", "05/12/2019", "http://cyber-trust.fr", "@#3DJJ3", "D1")

	checkGetAllDeviceLog(t, stub, "devicelog")
}
