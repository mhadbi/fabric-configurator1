package main

import (
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type CyberTrust struct {
	organization                  Organization
	device                        Device
	deviceClass                   DeviceClass
	administrator                 Administrator
	patchFile                     PatchFile
	patch                         Patch
	configurationFile             ConfigurationFile
	configuration                 Configuration
	deviceLog                     DeviceLog
	evidence                      Evidence
	evidenceFile                  EvidenceFile
	osEvidenceFile                OSEvidenceFile
	softwareEvidenceFile          SoftwareEvidenceFile
	networkDiagnosticEvidenceFile NetworkDiagnosticEvidenceFile
	networkTrafficEvidenceFile    NetworkTrafficEvidenceFile
	incidentTrackingEvidenceFile  IncidentTrackingEvidenceFile
}

type DeviceRights struct {
	DeviceID         Device
	OrganizationID   Organization
	Right            string
	DataSharingLevel string
}

func (t *CyberTrust) Init(stub shim.ChaincodeStubInterface) pb.Response {

	fmt.Println("Init")
	return shim.Success([]byte("Init success"))
}

func (t *CyberTrust) Invoke(stub shim.ChaincodeStubInterface) pb.Response {

	defer func() {
		if r := recover(); r != nil {
			functionnalError, ok := r.(FunctionnalError)
			if ok {
				shim.Error(functionnalError.errorTag)
			}
			err, ok := r.(error)
			if ok {
				shim.Error(fmt.Sprintf("%v", err))
			} else {
				shim.Error("unknownError")
			}
		}
	}()

	fc, args := stub.GetFunctionAndParameters()

	switch {
	// ORGANIZATION
	case strings.Compare(fc, "CreateOrganization") == 0:
		return t.organization.CreateOrganization(stub, args)

	case strings.Compare(fc, "GetOrganizationByID") == 0:
		return t.organization.GetOrganizationByID(stub, args[0])

	case strings.Compare(fc, "UpdateOrganizationByID") == 0:
		return t.organization.UpdateOrganizationByID(stub, args)

	case strings.Compare(fc, "UnregisterOrganizationByID") == 0:
		return t.organization.UnregisterOrganizationByID(stub, args)

	case strings.Compare(fc, "GetAllOrganizations") == 0:
		return t.organization.GetAllOrganizations(stub, args)

	case strings.Compare(fc, "GetAllOrganizationsByPage") == 0:
		return t.organization.GetAllOrganizationsByPage(stub, args)

	// DEVICE CLASS
	case strings.Compare(fc, "CreateDeviceClass") == 0:
		return t.deviceClass.CreateDeviceClass(stub, args)

	case strings.Compare(fc, "GetDeviceClassByID") == 0:
		return t.deviceClass.GetDeviceClassByID(stub, args)

	case strings.Compare(fc, "UnregisterDeviceClassByID") == 0:
		return t.deviceClass.UnregisterDeviceClassByID(stub, args)

	case strings.Compare(fc, "UpdateDeviceClassByID") == 0:
		return t.deviceClass.UpdateDeviceClassByID(stub, args)

	case strings.Compare(fc, "UpdateCurrentConfiguration") == 0:
		return UpdateCurrentConfiguration(stub, args)
	case strings.Compare(fc, "GetAllDeviceClass") == 0:
		return t.deviceClass.GetAllDeviceClass(stub, args)
	case strings.Compare(fc, "GetAllDeviceClassWithPage") == 0:
		return t.deviceClass.GetAllDeviceClassWithPage(stub, args)
	case strings.Compare(fc, "GetAllDeviceClassByOrganization") == 0:
		return t.deviceClass.GetAllDeviceClassByOrganization(stub, args)

	// DEVICE
	case strings.Compare(fc, "CreateDevice") == 0:
		return t.device.CreateDevice(stub, args)

	case strings.Compare(fc, "GetDeviceByID") == 0:
		return t.device.GetDeviceByID(stub, args)

	case strings.Compare(fc, "UnregisterDeviceByID") == 0:
		return t.device.UnregisterDeviceByID(stub, args)

	case strings.Compare(fc, "UpdateDeviceByID") == 0:
		return t.device.UpdateDeviceByID(stub, args)

	case strings.Compare(fc, "GetAllDevices") == 0:
		return t.device.GetAllDevices(stub, args)

	case strings.Compare(fc, "GetAllDevicesByPage") == 0:
		return t.device.GetAllDevicesByPage(stub, args)

	case strings.Compare(fc, "GetAllDeviceByDeviceClass") == 0:
		return t.device.GetAllDeviceByDeviceClass(stub, args)

	case strings.Compare(fc, "GetAllDeviceByOrganization") == 0:
		return t.device.GetAllDeviceByOrganization(stub, args)

	case strings.Compare(fc, "GetDevicesOwnedByEuCitizen") == 0:
		return t.device.GetDevicesOwnedByEuCitizen(stub, args)

	// ADMINISTRATOR
	case strings.Compare(fc, "CreateAdministrator") == 0:
		return t.administrator.CreateAdministrator(stub, args)

	case strings.Compare(fc, "GetAdministratorByEmployeeID") == 0:
		return t.administrator.GetAdministratorByEmployeeID(stub, args[0])

	case strings.Compare(fc, "UnregisterAdministratorByEmployeeID") == 0:
		return t.administrator.UnregisterAdministratorByEmployeeID(stub, args)

	case strings.Compare(fc, "UpdateAdministratorByID") == 0:
		return t.administrator.UpdateAdministratorByID(stub, args)

	case strings.Compare(fc, "GetAllAdministrators") == 0:
		return t.administrator.GetAllAdministrators(stub, args)

	case strings.Compare(fc, "GetAllAdministratorsByPage") == 0:
		return t.administrator.GetAllAdministratorsByPage(stub, args)

	case strings.Compare(fc, "GetAllAdministratorsByOrganization") == 0:
		return t.administrator.GetAllAdministratorsByOrganization(stub, args)
	// PATCHFILE
	case strings.Compare(fc, "CreatePatchFile") == 0:
		return t.patchFile.CreatePatchFile(stub, args)

	case strings.Compare(fc, "GetPatchFileByID") == 0:
		return t.patchFile.GetPatchFileByID(stub, args)

	case strings.Compare(fc, "UnregisterPatchFileByID") == 0:
		return UnregisterPatchFileByID(stub, args)
	case strings.Compare(fc, "GetAllPatchFiles") == 0:
		return t.patchFile.GetAllPatchFiles(stub, args)

	case strings.Compare(fc, "GetAllPatchFilesByPage") == 0:
		return t.patchFile.GetAllPatchFilesByPage(stub, args)
	case strings.Compare(fc, "UpdatePatchFileByID") == 0:
		return t.patchFile.UpdatePatchFileByID(stub, args)
	case strings.Compare(fc, "GetAllPatchFilesByPatch") == 0:
		return t.patchFile.GetAllPatchFilesByPatch(stub, args)

		// PATCH
	case strings.Compare(fc, "CreatePatch") == 0:
		return t.patch.CreatePatch(stub, args)

	case strings.Compare(fc, "GetPatchByID") == 0:
		return t.patch.GetPatchByID(stub, args)

	case strings.Compare(fc, "UpdatePatchByID") == 0:
		return t.patch.UpdatePatchByID(stub, args)

	case strings.Compare(fc, "UnregisterPatchByID") == 0:
		return UnregisterPatchByID(stub, args)

	case strings.Compare(fc, "GetAllPatchs") == 0:
		return t.patch.GetAllPatchs(stub, args)

	case strings.Compare(fc, "GetAllPatchsByPage") == 0:
		return t.patch.GetAllPatchsByPage(stub, args)
	case strings.Compare(fc, "GetAllPatchsByDeviceClass") == 0:
		return t.patch.GetAllPatchsByDeviceClass(stub, args)

	// CONFIGURATION FILE
	case strings.Compare(fc, "CreateConfigurationFile") == 0:
		return t.configurationFile.CreateConfigurationFile(stub, args)

	case strings.Compare(fc, "GetConfigurationFileByID") == 0:
		return t.configurationFile.GetConfigurationFileByID(stub, args[0])

	case strings.Compare(fc, "UpdateConfigurationFileByID") == 0:
		return t.configurationFile.UpdateConfigurationFileByID(stub, args)

	case strings.Compare(fc, "UnregisterConfigurationFileByID") == 0:
		return t.configurationFile.UnregisterConfigurationFileByID(stub, args)
	case strings.Compare(fc, "GetAllConfigurationFileByPage") == 0:
		return t.configurationFile.GetAllConfigurationFileByPage(stub, args)
	case strings.Compare(fc, "GetAllConfigurationFile") == 0:
		return t.configurationFile.GetAllConfigurationFile(stub, args)
	case strings.Compare(fc, "GetAllConfigurationFileByConfiguration") == 0:
		return t.configurationFile.GetAllConfigurationFileByConfiguration(stub, args)

	// CONFIGURATION
	case strings.Compare(fc, "CreateConfiguration") == 0:
		return t.configuration.CreateConfiguration(stub, args)

	case strings.Compare(fc, "GetConfigurationByID") == 0:
		return t.configuration.GetConfigurationByID(stub, args)

	case strings.Compare(fc, "UpdateConfigurationByID") == 0:
		return t.configuration.UpdateConfigurationByID(stub, args)

	case strings.Compare(fc, "UnregisterConfigurationByID") == 0:
		return t.configuration.UnregisterConfigurationByID(stub, args)

	case strings.Compare(fc, "GetAllConfiguration") == 0:
		return t.configuration.GetAllConfiguration(stub, args)
	case strings.Compare(fc, "GetAllConfigurationByPage") == 0:
		return t.configuration.GetAllConfigurationByPage(stub, args)
	case strings.Compare(fc, "GetAllConfigurationByDeviceClass") == 0:
		return t.configuration.GetAllConfigurationByDeviceClass(stub, args)

	// DEVICELOG
	case strings.Compare(fc, "CreateDeviceLog") == 0:
		return t.deviceLog.CreateDeviceLog(stub, args)

	case strings.Compare(fc, "GetDeviceLogByID") == 0:
		return t.deviceLog.GetDeviceLogByID(stub, args)

	case strings.Compare(fc, "GetAllDeviceLog") == 0:
		return t.deviceLog.GetAllDeviceLog(stub, args)

		// EVIDENCE
	case strings.Compare(fc, "CreateEvidence") == 0:
		return t.evidence.CreateEvidence(stub, args)

	case strings.Compare(fc, "GetEvidenceByID") == 0:
		return t.evidence.GetEvidenceByID(stub, args[0])

	case strings.Compare(fc, "GetAllEvidences") == 0:
		return t.evidence.GetAllEvidences(stub, args)

	case strings.Compare(fc, "GetAllEvidencesByPage") == 0:
		return t.evidence.GetAllEvidencesByPage(stub, args)

		//EVIDENCE FILE
	case strings.Compare(fc, "GetAllEvidenceFile") == 0:
		return t.evidenceFile.GetAllEvidenceFile(stub, args)
	case strings.Compare(fc, "GetAllEvidenceFileByEvidence") == 0:
		return t.evidenceFile.GetAllEvidenceFileByEvidence(stub, args)

		//INCIDENCE TRACKING EVIDENCE FILE

	case strings.Compare(fc, "CreateIncidentTrackingEvidenceFile") == 0:
		return t.incidentTrackingEvidenceFile.CreateIncidentTrackingEvidenceFile(stub, args)
	case strings.Compare(fc, "GetIncidentTrackingEvidenceFileByID") == 0:
		return t.incidentTrackingEvidenceFile.GetIncidentTrackingEvidenceFileByID(stub, args[0])
	case strings.Compare(fc, "GetAllIncidentTrackingEvidenceFile") == 0:
		return t.incidentTrackingEvidenceFile.GetAllIncidentTrackingEvidenceFile(stub, args)
	case strings.Compare(fc, "GetAllIncidentTrackingEvidenceFileByPage") == 0:
		return t.incidentTrackingEvidenceFile.GetAllIncidentTrackingEvidenceFileByPage(stub, args)

		//OS EVIDENCE FILE
	case strings.Compare(fc, "CreateOSEvidenceFile") == 0:
		return t.osEvidenceFile.CreateOSEvidenceFile(stub, args)
	case strings.Compare(fc, "GetOSEvidenceFileByID") == 0:
		return t.osEvidenceFile.GetOSEvidenceFileByID(stub, args[0])
	case strings.Compare(fc, "GetAllOSEvidenceFile") == 0:
		return t.osEvidenceFile.GetAllOSEvidenceFile(stub, args)
	case strings.Compare(fc, "GetAllOSEvidenceFileByPage") == 0:
		return t.osEvidenceFile.GetAllOSEvidenceFileByPage(stub, args)

		//NETWORK DIAGNOSTIC EVIDENCE FILE
	case strings.Compare(fc, "CreateNetworkDiagnosticEvidenceFile") == 0:
		return t.networkDiagnosticEvidenceFile.CreateNetworkDiagnosticEvidenceFile(stub, args)
	case strings.Compare(fc, "GetNetworkDiagnosticEvidenceFileByID") == 0:
		return t.networkDiagnosticEvidenceFile.GetNetworkDiagnosticEvidenceFileByID(stub, args[0])
	case strings.Compare(fc, "GetAllNetworkDiagnosticEvidenceFile") == 0:
		return t.networkDiagnosticEvidenceFile.GetAllNetworkDiagnosticEvidenceFile(stub, args)
	case strings.Compare(fc, "GetAllNetworkDiagnosticEvidenceFileByPage") == 0:
		return t.networkDiagnosticEvidenceFile.GetAllNetworkDiagnosticEvidenceFileByPage(stub, args)

		//NETWORK TRAFFIC EVIDENCE FILE
	case strings.Compare(fc, "CreateNetworkTrafficEvidenceFile") == 0:
		return t.networkTrafficEvidenceFile.CreateNetworkTrafficEvidenceFile(stub, args)
	case strings.Compare(fc, "GetNetworkTrafficEvidenceFileByID") == 0:
		return t.networkTrafficEvidenceFile.GetNetworkTrafficEvidenceFileByID(stub, args[0])
	case strings.Compare(fc, "GetAllNetworkTrafficEvidenceFile") == 0:
		return t.networkTrafficEvidenceFile.GetAllNetworkTrafficEvidenceFile(stub, args)
	case strings.Compare(fc, "GetAllNetworkTrafficEvidenceFileByPage") == 0:
		return t.networkTrafficEvidenceFile.GetAllNetworkTrafficEvidenceFileByPage(stub, args)

		//SOFTWARE EVIDENCE FILE
	case strings.Compare(fc, "CreateSoftwareEvidenceFile") == 0:
		return t.softwareEvidenceFile.CreateSoftwareEvidenceFile(stub, args)
	case strings.Compare(fc, "GetSoftwareEvidenceFileByID") == 0:
		return t.softwareEvidenceFile.GetSoftwareEvidenceFileByID(stub, args[0])
	case strings.Compare(fc, "GetAllSoftwareEvidenceFile") == 0:
		return t.softwareEvidenceFile.GetAllSoftwareEvidenceFile(stub, args)
	case strings.Compare(fc, "GetAllSoftwareEvidenceFileByPage") == 0:
		return t.softwareEvidenceFile.GetAllSoftwareEvidenceFileByPage(stub, args)

	default:
		return shim.Error("Called function is not defined in the chaincode ")
	}

}

func main() {
	err := shim.Start(new(CyberTrust))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}
