package main

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkGetExistingIncidentTrackingEvidenceFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Existing IncidentTrackingEvidenceFile " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetIncidentTrackingEvidenceFileByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	dc := IncidentTrackingEvidenceFile{}
	_ = json.Unmarshal(response.Payload, &dc)

	if dc.Uuid != uuid {
		t.Fail()
	}
}

func checkGetNonExistingIncidentTrackingEvidenceFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get NON Existing IncidentTrackingEvidenceFile " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetIncidentTrackingEvidenceFileByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkCreateIncidentTrackingEvidenceFileWhenEvidenceDoesNotExist(t *testing.T, stub *shim.MockStub, uuid string,
	name string, prettyName string, evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string,
	attackStatus string, evidence string) {
	displayNewTest("Create IncidentTrackingEvidenceFile when Evidence Org DOES NOT Exist")
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateIncidentTrackingEvidenceFile"), []byte(uuid), []byte(name), []byte(prettyName), []byte(evidenceFileDataSourceTitle),
		[]byte(evidenceFileInformation), []byte(evidenceFileCreationDate), []byte(attackStatus), []byte(evidence)})
	if response.Status == shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t *testing.T, stub *shim.MockStub, uuid string,
	name string, prettyName string, evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string,
	attackStatus string, evidence string) {
	displayNewTest("Create IncidentTrackingEvidenceFile when Evidence Exist")
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateIncidentTrackingEvidenceFile"), []byte(uuid), []byte(name), []byte(prettyName), []byte(evidenceFileDataSourceTitle),
		[]byte(evidenceFileInformation), []byte(evidenceFileCreationDate), []byte(attackStatus), []byte(evidence)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllIncidentTrackingEvidenceFile(t *testing.T, stub *shim.MockStub, objectType string, numberOfDeviceCreated int) {
	displayNewTest("GetAllByObjectType IncidentTrackingEvidenceFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllIncidentTrackingEvidenceFile"),
		[]byte(objectType)})

	var liste []IncidentTrackingEvidenceFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil || len(liste) != numberOfDeviceCreated {
		t.Fail()
	}
}

func checkGetAllIncidentTrackingEvidenceFileWithPage(t *testing.T, stub *shim.MockStub, pageSize string, offset string) {
	displayNewTest("GetAllByObjectTypeWithpage IncidentTrackingEvidenceFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllIncidentTrackingEvidenceFileByPage"), []byte(pageSize), []byte(offset)})
	var liste []IncidentTrackingEvidenceFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t, stub, "IE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E0")
}

func TestCreateIncidentTrackingEvidenceFileWhenEvidenceDoesNotExist(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceDoesNotExist(t, stub, "IE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E0")
}

func TestGetExistingIncidentTrackingEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t, stub, "IE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E0")

	checkGetExistingIncidentTrackingEvidenceFile(t, stub, "IE0")

}

func TestGetNonExistingIncidentTrackingEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	checkGetNonExistingIncidentTrackingEvidenceFile(t, stub, "IE0")
}

func TestGetAllIncidentTrackingEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t, stub, "IE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E0")
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t, stub, "IE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E0")

	checkGetAllIncidentTrackingEvidenceFile(t, stub, "incidentTrackingEvidenceFile", 2)
}

func TestGetAllIncidentTrackingEvidenceFileWithPage(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E1"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t, stub, "IE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E0")
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t, stub, "IE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E0")
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t, stub, "IE2", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E1")
	checkCreateIncidentTrackingEvidenceFileWhenEvidenceExist(t, stub, "IE3", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "E1")

	checkGetAllIncidentTrackingEvidenceFileWithPage(t, stub, "100", "0")
}
