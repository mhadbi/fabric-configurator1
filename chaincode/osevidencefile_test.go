package main

import (
	"encoding/json"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func checkGetExistingOSEvidenceFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get Existing OSEvidenceFile " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetOSEvidenceFileByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}

	dc := OSEvidenceFile{}
	_ = json.Unmarshal(response.Payload, &dc)

	if dc.Uuid != uuid {
		t.Fail()
	}
}

func checkGetNonExistingOSEvidenceFile(t *testing.T, stub *shim.MockStub, uuid string) {
	displayNewTest("Get NON Existing OSEvidenceFile " + uuid + " From Ledger Test")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetOSEvidenceFileByID"), []byte(uuid)})
	if response.Status != shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkCreateOSEvidenceFileWhenEvidenceDoesNotExist(t *testing.T, stub *shim.MockStub, uuid string,
	name string, prettyName string, evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string,
	attackStatus string, osName string, osVersion string, evidence string, deviceLogs []string) {
	displayNewTest("Create OSEvidenceFile when Evidence Org DOES NOT Exist")
	deviceLogsString := strings.Join(deviceLogs, ",")
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateOSEvidenceFile"), []byte(uuid), []byte(name), []byte(prettyName), []byte(evidenceFileDataSourceTitle),
		[]byte(evidenceFileInformation), []byte(evidenceFileCreationDate), []byte(attackStatus), []byte(osName), []byte(osVersion), []byte(evidence), []byte(deviceLogsString)})
	if response.Status == shim.OK || response.Payload != nil {
		t.Fail()
	}
}

func checkCreateOSEvidenceFileWhenEvidenceExist(t *testing.T, stub *shim.MockStub, uuid string,
	name string, prettyName string, evidenceFileDataSourceTitle string, evidenceFileInformation string, evidenceFileCreationDate string,
	attackStatus string, osName string, osVersion string, evidence string, deviceLogs []string) {
	displayNewTest("Create OSEvidenceFile when Evidence Exist")
	deviceLogsString := strings.Join(deviceLogs, ",")
	response := stub.MockInvoke("1", [][]byte{[]byte("CreateOSEvidenceFile"), []byte(uuid), []byte(name), []byte(prettyName), []byte(evidenceFileDataSourceTitle),
		[]byte(evidenceFileInformation), []byte(evidenceFileCreationDate), []byte(attackStatus), []byte(osName), []byte(osVersion), []byte(evidence), []byte(deviceLogsString)})
	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func checkGetAllOSEvidenceFile(t *testing.T, stub *shim.MockStub, objectType string, numberOfDeviceCreated int) {
	displayNewTest("GetAllByObjectType OSEvidenceFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllOSEvidenceFile"),
		[]byte(objectType)})

	var liste []OSEvidenceFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil || len(liste) != numberOfDeviceCreated {
		t.Fail()
	}
}

func checkGetAllOSEvidenceFileWithPage(t *testing.T, stub *shim.MockStub, pageSize string, offset string) {
	displayNewTest("GetAllByObjectTypeWithpage OSEvidenceFile ")

	response := stub.MockInvoke("1", [][]byte{[]byte("GetAllOSEvidenceFileByPage"), []byte(pageSize), []byte(offset)})
	var liste []OSEvidenceFile
	_ = json.Unmarshal(response.Payload, &liste)

	if response.Status != shim.OK || response.Payload == nil {
		t.Fail()
	}
}

func TestCreateOSEvidenceFileWhenEvidenceExist(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	mockTest(t, stub)
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte("DL1"), []byte("name"), []byte("typeOfDeviceLog"), []byte("format"), []byte("dateStart"),
		[]byte("dateEnd"), []byte("dateOfficial"), []byte("storageURL"), []byte("fileHash"), []byte("D1")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateOSEvidenceFileWhenEvidenceExist(t, stub, "OSE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E0", []string{"DL1"})
}

func TestCreateOSEvidenceFileWhenEvidenceDoesNotExist(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	mockTest(t, stub)
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte("DL1"), []byte("name"), []byte("typeOfDeviceLog"), []byte("format"), []byte("dateStart"),
		[]byte("dateEnd"), []byte("dateOfficial"), []byte("storageURL"), []byte("fileHash"), []byte("D1")})

	checkCreateOSEvidenceFileWhenEvidenceDoesNotExist(t, stub, "OSE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E0", []string{"DL1"})
}

func TestGetExistingOSEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	mockTest(t, stub)
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte("DL1"), []byte("name"), []byte("typeOfDeviceLog"), []byte("format"), []byte("dateStart"),
		[]byte("dateEnd"), []byte("dateOfficial"), []byte("storageURL"), []byte("fileHash"), []byte("D1")})

	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateOSEvidenceFileWhenEvidenceExist(t, stub, "OSE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E0", []string{"DL1"})

	checkGetExistingOSEvidenceFile(t, stub, "OSE0")

}

func TestGetNonExistingOSEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	checkGetNonExistingOSEvidenceFile(t, stub, "OSE0")
}

func TestGetAllOSEvidenceFile(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)

	mockTest(t, stub)
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte("DL1"), []byte("name"), []byte("typeOfDeviceLog"), []byte("format"), []byte("dateStart"),
		[]byte("dateEnd"), []byte("dateOfficial"), []byte("storageURL"), []byte("fileHash"), []byte("D1")})

	//Osregister
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateOSEvidenceFileWhenEvidenceExist(t, stub, "OSE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E0", []string{"DL1"})
	checkCreateOSEvidenceFileWhenEvidenceExist(t, stub, "OSE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E0", []string{"DL1"})

	checkGetAllOSEvidenceFile(t, stub, "evidenceFile", 2)
}

func TestGetAllOSEvidenceFileWithPage(t *testing.T) {
	scc := new(CyberTrust)
	stub := shim.NewMockStub("ex02", scc)
	mockTest(t, stub)
	stub.MockInvoke("1", [][]byte{[]byte("CreateDeviceLog"), []byte("DL1"), []byte("name"), []byte("typeOfDeviceLog"), []byte("format"), []byte("dateStart"),
		[]byte("dateEnd"), []byte("dateOfficial"), []byte("storageURL"), []byte("fileHash"), []byte("D1")})

	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E0"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	stub.MockInvoke("1", [][]byte{[]byte("CreateEvidence"), []byte("E1"), []byte("Evidence Test"), []byte("Evidence Test"), []byte("20/10/2019"), []byte("http://cyber-trust.fr/evidenceURL"),
		[]byte("TargetedVulnerability"), []byte("TypeOfAttack")})
	checkCreateOSEvidenceFileWhenEvidenceExist(t, stub, "OSE0", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E0", []string{"DL1"})
	checkCreateOSEvidenceFileWhenEvidenceExist(t, stub, "OSE1", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E0", []string{"DL1"})
	checkCreateOSEvidenceFileWhenEvidenceExist(t, stub, "IE2", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E1", []string{"DL1"})
	checkCreateOSEvidenceFileWhenEvidenceExist(t, stub, "IE3", "EvidenceFile Test", "EvidenceFile Test",
		"Evidence File Data Source Title", "Evidence File Information", "Evidence File Creation Date", "Attack status", "OSNAME", "OSVERSION", "E1", []string{"DL1"})

	checkGetAllOSEvidenceFileWithPage(t, stub, "100", "0")
}
